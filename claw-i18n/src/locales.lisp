;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; $Header: src/locales.lisp $

;;; Copyright (c) 2008, Andrea Chiumenti.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; --*-- AUTOMATICALLY GENERATED - DO NOT EDIT !!!!! --*--

(in-package :claw-i18n)

(setf (gethash :ja-jp *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "JPY")
        :DATE-FORMAT (list
                       :AMPM '("午前" "午後")
                       :MONTHS '("1月" "2月" "3月" "4月" "5月" "6月" "7月" "8月" "9月" "10月" "11月" "12月")
                       :SHORT-MONTHS '("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("日曜日" "月曜日" "火曜日" "水曜日" "木曜日" "金曜日" "土曜日")
                       :SHORT-WEEKDAYS '("日" "月" "火" "水" "木" "金" "土")
                       :ERAS '("紀元前" "西暦"))))

(setf (gethash :es-pe *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "PEN")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :en *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ja-jp *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "JPY")
        :DATE-FORMAT (list
                       :AMPM '("午前" "午後")
                       :MONTHS '("1月" "2月" "3月" "4月" "5月" "6月" "7月" "8月" "9月" "10月" "11月" "12月")
                       :SHORT-MONTHS '("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("日曜日" "月曜日" "火曜日" "水曜日" "木曜日" "金曜日" "土曜日")
                       :SHORT-WEEKDAYS '("日" "月" "火" "水" "木" "金" "土")
                       :ERAS '("紀元前" "西暦"))))

(setf (gethash "es_PA" *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "PAB")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :sr-ba *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "BAM")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("јануар" "фебруар" "март" "април" "мај" "јуни" "јули" "август" "септембар" "октобар" "новембар" "децембар")
                       :SHORT-MONTHS '("јан" "феб" "мар" "апр" "мај" "јун" "јул" "авг" "сеп" "окт" "нов" "дец")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("недеља" "понедељак" "уторак" "сриједа" "четвртак" "петак" "субота")
                       :SHORT-WEEKDAYS '("нед" "пон" "уто" "сри" "чет" "пет" "суб")
                       :ERAS '("п. н. е." "н. е"))))

(setf (gethash :mk *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("јануари" "февруари" "март" "април" "мај" "јуни" "јули" "август" "септември" "октомври" "ноември" "декември")
                       :SHORT-MONTHS '("јан." "фев." "мар." "апр." "мај." "јун." "јул." "авг." "септ." "окт." "ноем." "декем.")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("недела" "понеделник" "вторник" "среда" "четврток" "петок" "сабота")
                       :SHORT-WEEKDAYS '("нед." "пон." "вт." "сре." "чет." "пет." "саб.")
                       :ERAS '("пр.н.е." "ае."))))

(setf (gethash :es-gt *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "GTQ")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ar-ae *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "AED")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :no-no *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "NOK")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januar" "februar" "mars" "april" "mai" "juni" "juli" "august" "september" "oktober" "november" "desember")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "mai" "jun" "jul" "aug" "sep" "okt" "nov" "des")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("søndag" "mandag" "tirsdag" "onsdag" "torsdag" "fredag" "lørdag")
                       :SHORT-WEEKDAYS '("sø" "ma" "ti" "on" "to" "fr" "lø")
                       :ERAS '("BC" "AD"))))

(setf (gethash :sq-al *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "ALL")
        :DATE-FORMAT (list
                       :AMPM '("PD" "MD")
                       :MONTHS '("janar" "shkurt" "mars" "prill" "maj" "qershor" "korrik" "gusht" "shtator" "tetor" "nëntor" "dhjetor")
                       :SHORT-MONTHS '("Jan" "Shk" "Mar" "Pri" "Maj" "Qer" "Kor" "Gsh" "Sht" "Tet" "Nën" "Dhj")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("e diel" "e hënë" "e martë" "e mërkurë" "e enjte" "e premte" "e shtunë")
                       :SHORT-WEEKDAYS '("Die" "Hën" "Mar" "Mër" "Enj" "Pre" "Sht")
                       :ERAS '("p.e.r." "n.e.r."))))

(setf (gethash :bg *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Януари" "Февруари" "Март" "Април" "Май" "Юни" "Юли" "Август" "Септември" "Октомври" "Ноември" "Декември")
                       :SHORT-MONTHS '("I" "II" "III" "IV" "V" "VI" "VII" "VIII" "IX" "X" "XI" "XII")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Неделя" "Понеделник" "Вторник" "Сряда" "Четвъртък" "Петък" "Събота")
                       :SHORT-WEEKDAYS '("Нд" "Пн" "Вт" "Ср" "Чт" "Пт" "Сб")
                       :ERAS '("пр.н.е." "н.е."))))

(setf (gethash :ar-iq *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "IQD")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :ar-ye *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "YER")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :hu *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("DE" "DU")
                       :MONTHS '("január" "február" "március" "április" "május" "június" "július" "augusztus" "szeptember" "október" "november" "december")
                       :SHORT-MONTHS '("jan." "febr." "márc." "ápr." "máj." "jún." "júl." "aug." "szept." "okt." "nov." "dec.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("vasárnap" "hétfő" "kedd" "szerda" "csütörtök" "péntek" "szombat")
                       :SHORT-WEEKDAYS '("V" "H" "K" "Sze" "Cs" "P" "Szo")
                       :ERAS '("i.e." "i.u."))))

(setf (gethash :pt-pt *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Janeiro" "Fevereiro" "Março" "Abril" "Maio" "Junho" "Julho" "Agosto" "Setembro" "Outubro" "Novembro" "Dezembro")
                       :SHORT-MONTHS '("Jan" "Fev" "Mar" "Abr" "Mai" "Jun" "Jul" "Ago" "Set" "Out" "Nov" "Dez")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Domingo" "Segunda-feira" "Terça-feira" "Quarta-feira" "Quinta-feira" "Sexta-feira" "Sábado")
                       :SHORT-WEEKDAYS '("Dom" "Seg" "Ter" "Qua" "Qui" "Sex" "Sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :el-cy *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "CYP")
        :DATE-FORMAT (list
                       :AMPM '("ΠΜ" "ΜΜ")
                       :MONTHS '("Ιανουάριος" "Φεβρουάριος" "Μάρτιος" "Απρίλιος" "Μάιος" "Ιούνιος" "Ιούλιος" "Αύγουστος" "Σεπτέμβριος" "Οκτώβριος" "Νοέμβριος" "Δεκέμβριος")
                       :SHORT-MONTHS '("Ιαν" "Φεβ" "Μαρ" "Απρ" "Μαϊ" "Ιουν" "Ιουλ" "Αυγ" "Σεπ" "Οκτ" "Νοε" "Δεκ")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Κυριακή" "Δευτέρα" "Τρίτη" "Τετάρτη" "Πέμπτη" "Παρασκευή" "Σάββατο")
                       :SHORT-WEEKDAYS '("Κυρ" "Δευ" "Τρι" "Τετ" "Πεμ" "Παρ" "Σαβ")
                       :ERAS '("π.Χ." "μ.Χ."))))

(setf (gethash :ar-qa *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "QAR")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :mk-mk *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "MKD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("јануари" "февруари" "март" "април" "мај" "јуни" "јули" "август" "септември" "октомври" "ноември" "декември")
                       :SHORT-MONTHS '("јан." "фев." "мар." "апр." "мај." "јун." "јул." "авг." "септ." "окт." "ноем." "декем.")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("недела" "понеделник" "вторник" "среда" "четврток" "петок" "сабота")
                       :SHORT-WEEKDAYS '("нед." "пон." "вт." "сре." "чет." "пет." "саб.")
                       :ERAS '("пр.н.е." "ае."))))

(setf (gethash :sv *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januari" "februari" "mars" "april" "maj" "juni" "juli" "augusti" "september" "oktober" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "maj" "jun" "jul" "aug" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("söndag" "måndag" "tisdag" "onsdag" "torsdag" "fredag" "lördag")
                       :SHORT-WEEKDAYS '("sö" "må" "ti" "on" "to" "fr" "lö")
                       :ERAS '("BC" "AD"))))

(setf (gethash :de-ch *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\' :DECIMAL-SEPARATOR #\. "CHF")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Januar" "Februar" "März" "April" "Mai" "Juni" "Juli" "August" "September" "Oktober" "November" "Dezember")
                       :SHORT-MONTHS '("Jan" "Feb" "Mrz" "Apr" "Mai" "Jun" "Jul" "Aug" "Sep" "Okt" "Nov" "Dez")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Sonntag" "Montag" "Dienstag" "Mittwoch" "Donnerstag" "Freitag" "Samstag")
                       :SHORT-WEEKDAYS '("So" "Mo" "Di" "Mi" "Do" "Fr" "Sa")
                       :ERAS '("v. Chr." "n. Chr."))))

(setf (gethash :en-us *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "USD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :fi-fi *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("tammikuu" "helmikuu" "maaliskuu" "huhtikuu" "toukokuu" "kesäkuu" "heinäkuu" "elokuu" "syyskuu" "lokakuu" "marraskuu" "joulukuu")
                       :SHORT-MONTHS '("tammi" "helmi" "maalis" "huhti" "touko" "kesä" "heinä" "elo" "syys" "loka" "marras" "joulu")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("sunnuntai" "maanantai" "tiistai" "keskiviikko" "torstai" "perjantai" "lauantai")
                       :SHORT-WEEKDAYS '("su" "ma" "ti" "ke" "to" "pe" "la")
                       :ERAS '("BC" "AD"))))

(setf (gethash :is *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janúar" "febrúar" "mars" "apríl" "maí" "júní" "júlí" "ágúst" "september" "október" "nóvember" "desember")
                       :SHORT-MONTHS '("jan." "feb." "mar." "apr." "maí" "jún." "júl." "ágú." "sep." "okt." "nóv." "des.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("sunnudagur" "mánudagur" "þriðjudagur" "miðvikudagur" "fimmtudagur" "föstudagur" "laugardagur")
                       :SHORT-WEEKDAYS '("sun." "mán." "þri." "mið." "fim." "fös." "lau.")
                       :ERAS '("BC" "AD"))))

(setf (gethash :cs *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("dop." "odp.")
                       :MONTHS '("leden" "únor" "březen" "duben" "květen" "červen" "červenec" "srpen" "září" "říjen" "listopad" "prosinec")
                       :SHORT-MONTHS '("I" "II" "III" "IV" "V" "VI" "VII" "VIII" "IX" "X" "XI" "XII")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Neděle" "Pondělí" "Úterý" "Středa" "Čtvrtek" "Pátek" "Sobota")
                       :SHORT-WEEKDAYS '("Ne" "Po" "Út" "St" "Čt" "Pá" "So")
                       :ERAS '("př.Kr." "po Kr."))))

(setf (gethash :en-mt *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "MTL")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :sl-si *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januar" "februar" "marec" "april" "maj" "junij" "julij" "avgust" "september" "oktober" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "maj" "jun" "jul" "avg" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Nedelja" "Ponedeljek" "Torek" "Sreda" "Četrtek" "Petek" "Sobota")
                       :SHORT-WEEKDAYS '("Ned" "Pon" "Tor" "Sre" "Čet" "Pet" "Sob")
                       :ERAS '("pr.n.š." "po Kr."))))

(setf (gethash :sk-sk *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "SKK")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("január" "február" "marec" "apríl" "máj" "jún" "júl" "august" "september" "október" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "máj" "jún" "júl" "aug" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Nedeľa" "Pondelok" "Utorok" "Streda" "Štvrtok" "Piatok" "Sobota")
                       :SHORT-WEEKDAYS '("Ne" "Po" "Ut" "St" "Št" "Pi" "So")
                       :ERAS '("pred n.l." "n.l."))))

(setf (gethash :it *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("gennaio" "febbraio" "marzo" "aprile" "maggio" "giugno" "luglio" "agosto" "settembre" "ottobre" "novembre" "dicembre")
                       :SHORT-MONTHS '("gen" "feb" "mar" "apr" "mag" "giu" "lug" "ago" "set" "ott" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domenica" "lunedì" "martedì" "mercoledì" "giovedì" "venerdì" "sabato")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mer" "gio" "ven" "sab")
                       :ERAS '("BC" "dopo Cristo"))))

(setf (gethash :tr-tr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "TRY")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Ocak" "Şubat" "Mart" "Nisan" "Mayıs" "Haziran" "Temmuz" "Ağustos" "Eylül" "Ekim" "Kasım" "Aralık")
                       :SHORT-MONTHS '("Oca" "Şub" "Mar" "Nis" "May" "Haz" "Tem" "Ağu" "Eyl" "Eki" "Kas" "Ara")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Pazar" "Pazartesi" "Salı" "Çarşamba" "Perşembe" "Cuma" "Cumartesi")
                       :SHORT-WEEKDAYS '("Paz" "Pzt" "Sal" "Çar" "Per" "Cum" "Cmt")
                       :ERAS '("BC" "AD"))))

(setf (gethash :zh *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("上午" "下午")
                       :MONTHS '("一月" "二月" "三月" "四月" "五月" "六月" "七月" "八月" "九月" "十月" "十一月" "十二月")
                       :SHORT-MONTHS '("一月" "二月" "三月" "四月" "五月" "六月" "七月" "八月" "九月" "十月" "十一月" "十二月")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("星期日" "星期一" "星期二" "星期三" "星期四" "星期五" "星期六")
                       :SHORT-WEEKDAYS '("星期日" "星期一" "星期二" "星期三" "星期四" "星期五" "星期六")
                       :ERAS '("公元前" "公元"))))

(setf (gethash :th *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("ก่อนเที่ยง" "หลังเที่ยง")
                       :MONTHS '("มกราคม" "กุมภาพันธ์" "มีนาคม" "เมษายน" "พฤษภาคม" "มิถุนายน" "กรกฎาคม" "สิงหาคม" "กันยายน" "ตุลาคม" "พฤศจิกายน" "ธันวาคม")
                       :SHORT-MONTHS '("ม.ค." "ก.พ." "มี.ค." "เม.ย." "พ.ค." "มิ.ย." "ก.ค." "ส.ค." "ก.ย." "ต.ค." "พ.ย." "ธ.ค.")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("วันอาทิตย์" "วันจันทร์" "วันอังคาร" "วันพุธ" "วันพฤหัสบดี" "วันศุกร์" "วันเสาร์")
                       :SHORT-WEEKDAYS '("อา." "จ." "อ." "พ." "พฤ." "ศ." "ส.")
                       :ERAS '("ปีก่อนคริสต์กาลที่" "ค.ศ."))))

(setf (gethash :ar-sa *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "SAR")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :no *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januar" "februar" "mars" "april" "mai" "juni" "juli" "august" "september" "oktober" "november" "desember")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "mai" "jun" "jul" "aug" "sep" "okt" "nov" "des")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("søndag" "mandag" "tirsdag" "onsdag" "torsdag" "fredag" "lørdag")
                       :SHORT-WEEKDAYS '("sø" "ma" "ti" "on" "to" "fr" "lø")
                       :ERAS '("BC" "AD"))))

(setf (gethash :en-gb *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "GBP")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :sr-cs *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "CSD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("јануар" "фебруар" "март" "април" "мај" "јун" "јул" "август" "септембар" "октобар" "новембар" "децембар")
                       :SHORT-MONTHS '("јан" "феб" "мар" "апр" "мај" "јун" "јул" "авг" "сеп" "окт" "нов" "дец")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("недеља" "понедељак" "уторак" "среда" "четвртак" "петак" "субота")
                       :SHORT-WEEKDAYS '("нед" "пон" "уто" "сре" "чет" "пет" "суб")
                       :ERAS '("п. н. е." "н. е"))))

(setf (gethash :lt *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Sausio" "Vasario" "Kovo" "Balandžio" "Gegužės" "Birželio" "Liepos" "Rugpjūčio" "Rugsėjo" "Spalio" "Lapkričio" "Gruodžio")
                       :SHORT-MONTHS '("Sau" "Vas" "Kov" "Bal" "Geg" "Bir" "Lie" "Rgp" "Rgs" "Spa" "Lap" "Grd")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Sekmadienis" "Pirmadienis" "Antradienis" "Trečiadienis" "Ketvirtadienis" "Penktadienis" "Šeštadienis")
                       :SHORT-WEEKDAYS '("Sk" "Pr" "An" "Tr" "Kt" "Pn" "Št")
                       :ERAS '("pr.Kr." "po.Kr."))))

(setf (gethash :ro *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("ianuarie" "februarie" "martie" "aprilie" "mai" "iunie" "iulie" "august" "septembrie" "octombrie" "noiembrie" "decembrie")
                       :SHORT-MONTHS '("Ian" "Feb" "Mar" "Apr" "Mai" "Iun" "Iul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("duminică" "luni" "marţi" "miercuri" "joi" "vineri" "sîmbătă")
                       :SHORT-WEEKDAYS '("D" "L" "Ma" "Mi" "J" "V" "S")
                       :ERAS '("d.C." "î.d.C."))))

(setf (gethash :en-nz *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "NZD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :no-no *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "NOK")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januar" "februar" "mars" "april" "mai" "juni" "juli" "august" "september" "oktober" "november" "desember")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "mai" "jun" "jul" "aug" "sep" "okt" "nov" "des")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("søndag" "mandag" "tirsdag" "onsdag" "torsdag" "fredag" "lørdag")
                       :SHORT-WEEKDAYS '("sø" "ma" "ti" "on" "to" "fr" "lø")
                       :ERAS '("BC" "AD"))))

(setf (gethash :lt-lt *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "LTL")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Sausio" "Vasario" "Kovo" "Balandžio" "Gegužės" "Birželio" "Liepos" "Rugpjūčio" "Rugsėjo" "Spalio" "Lapkričio" "Gruodžio")
                       :SHORT-MONTHS '("Sau" "Vas" "Kov" "Bal" "Geg" "Bir" "Lie" "Rgp" "Rgs" "Spa" "Lap" "Grd")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Sekmadienis" "Pirmadienis" "Antradienis" "Trečiadienis" "Ketvirtadienis" "Penktadienis" "Šeštadienis")
                       :SHORT-WEEKDAYS '("Sk" "Pr" "An" "Tr" "Kt" "Pn" "Št")
                       :ERAS '("pr.Kr." "po.Kr."))))

(setf (gethash :es-ni *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "NIO")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :nl *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januari" "februari" "maart" "april" "mei" "juni" "juli" "augustus" "september" "oktober" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mrt" "apr" "mei" "jun" "jul" "aug" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("zondag" "maandag" "dinsdag" "woensdag" "donderdag" "vrijdag" "zaterdag")
                       :SHORT-WEEKDAYS '("zo" "ma" "di" "wo" "do" "vr" "za")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ga-ie *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "EUR")
        :DATE-FORMAT (list
                       :AMPM '("a.m." "p.m.")
                       :MONTHS '("Eanáir" "Feabhra" "Márta" "Aibreán" "Bealtaine" "Meitheamh" "Iúil" "Lúnasa" "Meán Fómhair" "Deireadh Fómhair" "Samhain" "Nollaig")
                       :SHORT-MONTHS '("Ean" "Feabh" "Márta" "Aib" "Beal" "Meith" "Iúil" "Lún" "MFómh" "DFómh" "Samh" "Noll")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Dé Domhnaigh" "Dé Luain" "Dé Máirt" "Dé Céadaoin" "Déardaoin" "Dé hAoine" "Dé Sathairn")
                       :SHORT-WEEKDAYS '("Domh" "Luan" "Máirt" "Céad" "Déar" "Aoine" "Sath")
                       :ERAS '("RC" "AD"))))

(setf (gethash :fr-be *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janvier" "février" "mars" "avril" "mai" "juin" "juillet" "août" "septembre" "octobre" "novembre" "décembre")
                       :SHORT-MONTHS '("janv." "févr." "mars" "avr." "mai" "juin" "juil." "août" "sept." "oct." "nov." "déc.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("dimanche" "lundi" "mardi" "mercredi" "jeudi" "vendredi" "samedi")
                       :SHORT-WEEKDAYS '("dim." "lun." "mar." "mer." "jeu." "ven." "sam.")
                       :ERAS '("BC" "ap. J.-C."))))

(setf (gethash :es-es *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ar-lb *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "LBP")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("كانون الثاني" "شباط" "آذار" "نيسان" "نوار" "حزيران" "تموز" "آب" "أيلول" "تشرين الأول" "تشرين الثاني" "كانون الأول")
                       :SHORT-MONTHS '("كانون الثاني" "شباط" "آذار" "نيسان" "نوار" "حزيران" "تموز" "آب" "أيلول" "تشرين الأول" "تشرين الثاني" "كانون الأول")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :ko *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("오전" "오후")
                       :MONTHS '("1월" "2월" "3월" "4월" "5월" "6월" "7월" "8월" "9월" "10월" "11월" "12월")
                       :SHORT-MONTHS '("1월" "2월" "3월" "4월" "5월" "6월" "7월" "8월" "9월" "10월" "11월" "12월")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("일요일" "월요일" "화요일" "수요일" "목요일" "금요일" "토요일")
                       :SHORT-WEEKDAYS '("일" "월" "화" "수" "목" "금" "토")
                       :ERAS '("BC" "AD"))))

(setf (gethash :fr-ca *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "CAD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janvier" "février" "mars" "avril" "mai" "juin" "juillet" "août" "septembre" "octobre" "novembre" "décembre")
                       :SHORT-MONTHS '("janv." "févr." "mars" "avr." "mai" "juin" "juil." "août" "sept." "oct." "nov." "déc.")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("dimanche" "lundi" "mardi" "mercredi" "jeudi" "vendredi" "samedi")
                       :SHORT-WEEKDAYS '("dim." "lun." "mar." "mer." "jeu." "ven." "sam.")
                       :ERAS '("BC" "ap. J.-C."))))

(setf (gethash :et-ee *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "EEK")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Jaanuar" "Veebruar" "Märts" "Aprill" "Mai" "Juuni" "Juuli" "August" "September" "Oktoober" "November" "Detsember")
                       :SHORT-MONTHS '("Jaan" "Veebr" "Märts" "Apr" "Mai" "Juuni" "Juuli" "Aug" "Sept" "Okt" "Nov" "Dets")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("pühapäev" "esmaspäev" "teisipäev" "kolmapäev" "neljapäev" "reede" "laupäev")
                       :SHORT-WEEKDAYS '("P" "E" "T" "K" "N" "R" "L")
                       :ERAS '("e.m.a." "m.a.j."))))

(setf (gethash :ar-kw *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "KWD")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :sr-rs *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "RSD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("јануар" "фебруар" "март" "април" "мај" "јун" "јул" "август" "септембар" "октобар" "новембар" "децембар")
                       :SHORT-MONTHS '("јан" "феб" "мар" "апр" "мај" "јун" "јул" "авг" "сеп" "окт" "нов" "дец")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("недеља" "понедељак" "уторак" "среда" "четвртак" "петак" "субота")
                       :SHORT-WEEKDAYS '("нед" "пон" "уто" "сре" "чет" "пет" "суб")
                       :ERAS '("п. н. е." "н. е"))))

(setf (gethash :es-us *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "USD")
        :DATE-FORMAT (list
                       :AMPM '("a.m." "p.m.")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("a.C." "d.C."))))

(setf (gethash :es-mx *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "MXN")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ar-sd *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "SDD")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :in-id *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "IDR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Januari" "Februari" "Maret" "April" "Mei" "Juni" "Juli" "Agustus" "September" "Oktober" "November" "Desember")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "Mei" "Jun" "Jul" "Agu" "Sep" "Okt" "Nov" "Des")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Minggu" "Senin" "Selasa" "Rabu" "Kamis" "Jumat" "Sabtu")
                       :SHORT-WEEKDAYS '("Min" "Sen" "Sel" "Rab" "Kam" "Jum" "Sab")
                       :ERAS '("BCE" "CE"))))

(setf (gethash :ru *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Январь" "Февраль" "Март" "Апрель" "Май" "Июнь" "Июль" "Август" "Сентябрь" "Октябрь" "Ноябрь" "Декабрь")
                       :SHORT-MONTHS '("янв" "фев" "мар" "апр" "май" "июн" "июл" "авг" "сен" "окт" "ноя" "дек")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("воскресенье" "понедельник" "вторник" "среда" "четверг" "пятница" "суббота")
                       :SHORT-WEEKDAYS '("Вс" "Пн" "Вт" "Ср" "Чт" "Пт" "Сб")
                       :ERAS '("до н.э." "н.э."))))

(setf (gethash :lv *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janvāris" "februāris" "marts" "aprīlis" "maijs" "jūnijs" "jūlijs" "augusts" "septembris" "oktobris" "novembris" "decembris")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "Maijs" "Jūn" "Jūl" "Aug" "Sep" "Okt" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("svētdiena" "pirmdiena" "otrdiena" "trešdiena" "ceturtdiena" "piektdiena" "sestdiena")
                       :SHORT-WEEKDAYS '("Sv" "P" "O" "T" "C" "Pk" "S")
                       :ERAS '("pmē" "mē"))))

(setf (gethash :es-uy *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "UYU")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :lv-lv *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "LVL")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janvāris" "februāris" "marts" "aprīlis" "maijs" "jūnijs" "jūlijs" "augusts" "septembris" "oktobris" "novembris" "decembris")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "Maijs" "Jūn" "Jūl" "Aug" "Sep" "Okt" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("svētdiena" "pirmdiena" "otrdiena" "trešdiena" "ceturtdiena" "piektdiena" "sestdiena")
                       :SHORT-WEEKDAYS '("Sv" "P" "O" "T" "C" "Pk" "S")
                       :ERAS '("pmē" "mē"))))

(setf (gethash :iw *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("ינואר" "פברואר" "מרץ" "אפריל" "מאי" "יוני" "יולי" "אוגוסט" "ספטמבר" "אוקטובר" "נובמבר" "דצמבר")
                       :SHORT-MONTHS '("ינו" "פבר" "מרץ" "אפר" "מאי" "יונ" "יול" "אוג" "ספט" "אוק" "נוב" "דצמ")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("יום ראשון" "יום שני" "יום שלישי" "יום רביעי" "יום חמישי" "יום שישי" "שבת")
                       :SHORT-WEEKDAYS '("א" "ב" "ג" "ד" "ה" "ו" "ש")
                       :ERAS '("לסה"נ" "לפסה"נ"))))

(setf (gethash :pt-br *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "BRL")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Janeiro" "Fevereiro" "Março" "Abril" "Maio" "Junho" "Julho" "Agosto" "Setembro" "Outubro" "Novembro" "Dezembro")
                       :SHORT-MONTHS '("Jan" "Fev" "Mar" "Abr" "Mai" "Jun" "Jul" "Ago" "Set" "Out" "Nov" "Dez")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Domingo" "Segunda-feira" "Terça-feira" "Quarta-feira" "Quinta-feira" "Sexta-feira" "Sábado")
                       :SHORT-WEEKDAYS '("Dom" "Seg" "Ter" "Qua" "Qui" "Sex" "Sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ar-sy *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "SYP")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("كانون الثاني" "شباط" "آذار" "نيسان" "نواران" "حزير" "تموز" "آب" "أيلول" "تشرين الأول" "تشرين الثاني" "كانون الأول")
                       :SHORT-MONTHS '("كانون الثاني" "شباط" "آذار" "نيسان" "نوار" "حزيران" "تموز" "آب" "أيلول" "تشرين الأول" "تشرين الثاني" "كانون الأول")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :hr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("siječanj" "veljača" "ožujak" "travanj" "svibanj" "lipanj" "srpanj" "kolovoz" "rujan" "listopad" "studeni" "prosinac")
                       :SHORT-MONTHS '("sij" "vel" "ožu" "tra" "svi" "lip" "srp" "kol" "ruj" "lis" "stu" "pro")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("nedjelja" "ponedjeljak" "utorak" "srijeda" "četvrtak" "petak" "subota")
                       :SHORT-WEEKDAYS '("ned" "pon" "uto" "sri" "čet" "pet" "sub")
                       :ERAS '("BC" "AD"))))

(setf (gethash :et *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Jaanuar" "Veebruar" "Märts" "Aprill" "Mai" "Juuni" "Juuli" "August" "September" "Oktoober" "November" "Detsember")
                       :SHORT-MONTHS '("Jaan" "Veebr" "Märts" "Apr" "Mai" "Juuni" "Juuli" "Aug" "Sept" "Okt" "Nov" "Dets")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("pühapäev" "esmaspäev" "teisipäev" "kolmapäev" "neljapäev" "reede" "laupäev")
                       :SHORT-WEEKDAYS '("P" "E" "T" "K" "N" "R" "L")
                       :ERAS '("e.m.a." "m.a.j."))))

(setf (gethash :es-do *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "DOP")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :fr-ch *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\' :DECIMAL-SEPARATOR #\. "CHF")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janvier" "février" "mars" "avril" "mai" "juin" "juillet" "août" "septembre" "octobre" "novembre" "décembre")
                       :SHORT-MONTHS '("janv." "févr." "mars" "avr." "mai" "juin" "juil." "août" "sept." "oct." "nov." "déc.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("dimanche" "lundi" "mardi" "mercredi" "jeudi" "vendredi" "samedi")
                       :SHORT-WEEKDAYS '("dim." "lun." "mar." "mer." "jeu." "ven." "sam.")
                       :ERAS '("BC" "ap. J.-C."))))

(setf (gethash :hi-in *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "INR")
        :DATE-FORMAT (list
                       :AMPM '("पूर्वाह्न" "अपराह्न")
                       :MONTHS '("जनवरी" "फ़रवरी" "मार्च" "अप्रैल" "मई" "जून" "जुलाई" "अगस्त" "सितंबर" "अक्‍तूबर" "नवंबर" "दिसंबर")
                       :SHORT-MONTHS '("जनवरी" "फ़रवरी" "मार्च" "अप्रैल" "मई" "जून" "जुलाई" "अगस्त" "सितंबर" "अक्‍तूबर" "नवंबर" "दिसंबर")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("रविवार" "सोमवार" "मंगलवार" "बुधवार" "गुरुवार" "शुक्रवार" "शनिवार")
                       :SHORT-WEEKDAYS '("रवि" "सोम" "मंगल" "बुध" "गुरु" "शुक्र" "शनि")
                       :ERAS '("ईसापूर्व" "सन"))))

(setf (gethash :es-ve *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "VEB")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ar-bh *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "BHD")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :en-ph *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "PHP")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ar-tn *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "TND")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :fi *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("tammikuu" "helmikuu" "maaliskuu" "huhtikuu" "toukokuu" "kesäkuu" "heinäkuu" "elokuu" "syyskuu" "lokakuu" "marraskuu" "joulukuu")
                       :SHORT-MONTHS '("tammi" "helmi" "maalis" "huhti" "touko" "kesä" "heinä" "elo" "syys" "loka" "marras" "joulu")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("sunnuntai" "maanantai" "tiistai" "keskiviikko" "torstai" "perjantai" "lauantai")
                       :SHORT-WEEKDAYS '("su" "ma" "ti" "ke" "to" "pe" "la")
                       :ERAS '("BC" "AD"))))

(setf (gethash :de-at *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Jänner" "Februar" "März" "April" "Mai" "Juni" "Juli" "August" "September" "Oktober" "November" "Dezember")
                       :SHORT-MONTHS '("Jän" "Feb" "Mär" "Apr" "Mai" "Jun" "Jul" "Aug" "Sep" "Okt" "Nov" "Dez")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Sonntag" "Montag" "Dienstag" "Mittwoch" "Donnerstag" "Freitag" "Samstag")
                       :SHORT-WEEKDAYS '("So" "Mo" "Di" "Mi" "Do" "Fr" "Sa")
                       :ERAS '("v. Chr." "n. Chr."))))

(setf (gethash :es *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :nl-nl *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januari" "februari" "maart" "april" "mei" "juni" "juli" "augustus" "september" "oktober" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mrt" "apr" "mei" "jun" "jul" "aug" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("zondag" "maandag" "dinsdag" "woensdag" "donderdag" "vrijdag" "zaterdag")
                       :SHORT-WEEKDAYS '("zo" "ma" "di" "wo" "do" "vr" "za")
                       :ERAS '("BC" "AD"))))

(setf (gethash :es-ec *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "USD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :zh-tw *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "TWD")
        :DATE-FORMAT (list
                       :AMPM '("上午" "下午")
                       :MONTHS '("一月" "二月" "三月" "四月" "五月" "六月" "七月" "八月" "九月" "十月" "十一月" "十二月")
                       :SHORT-MONTHS '("一月" "二月" "三月" "四月" "五月" "六月" "七月" "八月" "九月" "十月" "十一月" "十二月")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("星期日" "星期一" "星期二" "星期三" "星期四" "星期五" "星期六")
                       :SHORT-WEEKDAYS '("星期日" "星期一" "星期二" "星期三" "星期四" "星期五" "星期六")
                       :ERAS '("西元前" "西元"))))

(setf (gethash :ar-jo *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "JOD")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("كانون الثاني" "شباط" "آذار" "نيسان" "نوار" "حزيران" "تموز" "آب" "أيلول" "تشرين الأول" "تشرين الثاني" "كانون الأول")
                       :SHORT-MONTHS '("كانون الثاني" "شباط" "آذار" "نيسان" "نوار" "حزيران" "تموز" "آب" "أيلول" "تشرين الأول" "تشرين الثاني" "كانون الأول")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :be *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("студзеня" "лютага" "сакавіка" "красавіка" "мая" "чрвеня" "ліпеня" "жніўня" "верасня" "кастрычніка" "листапада" "снежня")
                       :SHORT-MONTHS '("стд" "лют" "скв" "крс" "май" "чрв" "лпн" "жнв" "врс" "кст" "лст" "снж")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("нядзеля" "панядзелак" "аўторак" "серада" "чацвер" "пятніца" "субота")
                       :SHORT-WEEKDAYS '("нд" "пн" "ат" "ср" "чц" "пт" "сб")
                       :ERAS '("да н.е." "н.е."))))

(setf (gethash :is-is *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "ISK")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janúar" "febrúar" "mars" "apríl" "maí" "júní" "júlí" "ágúst" "september" "október" "nóvember" "desember")
                       :SHORT-MONTHS '("jan." "feb." "mar." "apr." "maí" "jún." "júl." "ágú." "sep." "okt." "nóv." "des.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("sunnudagur" "mánudagur" "þriðjudagur" "miðvikudagur" "fimmtudagur" "föstudagur" "laugardagur")
                       :SHORT-WEEKDAYS '("sun." "mán." "þri." "mið." "fim." "fös." "lau.")
                       :ERAS '("BC" "AD"))))

(setf (gethash :es-co *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "COP")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :es-cr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "CRC")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :es-cl *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "CLP")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ar-eg *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "EGP")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :en-za *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "ZAR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :th-th *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "THB")
        :DATE-FORMAT (list
                       :AMPM '("ก่อนเที่ยง" "หลังเที่ยง")
                       :MONTHS '("มกราคม" "กุมภาพันธ์" "มีนาคม" "เมษายน" "พฤษภาคม" "มิถุนายน" "กรกฎาคม" "สิงหาคม" "กันยายน" "ตุลาคม" "พฤศจิกายน" "ธันวาคม")
                       :SHORT-MONTHS '("ม.ค." "ก.พ." "มี.ค." "เม.ย." "พ.ค." "มิ.ย." "ก.ค." "ส.ค." "ก.ย." "ต.ค." "พ.ย." "ธ.ค.")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("วันอาทิตย์" "วันจันทร์" "วันอังคาร" "วันพุธ" "วันพฤหัสบดี" "วันศุกร์" "วันเสาร์")
                       :SHORT-WEEKDAYS '("อา." "จ." "อ." "พ." "พฤ." "ศ." "ส.")
                       :ERAS '("ปีก่อนคริสต์กาลที่" "ค.ศ."))))

(setf (gethash :el-gr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("πμ" "μμ")
                       :MONTHS '("Ιανουάριος" "Φεβρουάριος" "Μάρτιος" "Απρίλιος" "Μάϊος" "Ιούνιος" "Ιούλιος" "Αύγουστος" "Σεπτέμβριος" "Οκτώβριος" "Νοέμβριος" "Δεκέμβριος")
                       :SHORT-MONTHS '("Ιαν" "Φεβ" "Μαρ" "Απρ" "Μαϊ" "Ιουν" "Ιουλ" "Αυγ" "Σεπ" "Οκτ" "Νοε" "Δεκ")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Κυριακή" "Δευτέρα" "Τρίτη" "Τετάρτη" "Πέμπτη" "Παρασκευή" "Σάββατο")
                       :SHORT-WEEKDAYS '("Κυρ" "Δευ" "Τρι" "Τετ" "Πεμ" "Παρ" "Σαβ")
                       :ERAS '("BC" "AD"))))

(setf (gethash :it-it *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("gennaio" "febbraio" "marzo" "aprile" "maggio" "giugno" "luglio" "agosto" "settembre" "ottobre" "novembre" "dicembre")
                       :SHORT-MONTHS '("gen" "feb" "mar" "apr" "mag" "giu" "lug" "ago" "set" "ott" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domenica" "lunedì" "martedì" "mercoledì" "giovedì" "venerdì" "sabato")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mer" "gio" "ven" "sab")
                       :ERAS '("BC" "dopo Cristo"))))

(setf (gethash :ca *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("gener" "febrer" "març" "abril" "maig" "juny" "juliol" "agost" "setembre" "octubre" "novembre" "desembre")
                       :SHORT-MONTHS '("gen." "feb." "març" "abr." "maig" "juny" "jul." "ag." "set." "oct." "nov." "des.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("diumenge" "dilluns" "dimarts" "dimecres" "dijous" "divendres" "dissabte")
                       :SHORT-WEEKDAYS '("dg." "dl." "dt." "dc." "dj." "dv." "ds.")
                       :ERAS '("BC" "AD"))))

(setf (gethash :hu-hu *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "HUF")
        :DATE-FORMAT (list
                       :AMPM '("DE" "DU")
                       :MONTHS '("január" "február" "március" "április" "május" "június" "július" "augusztus" "szeptember" "október" "november" "december")
                       :SHORT-MONTHS '("jan." "febr." "márc." "ápr." "máj." "jún." "júl." "aug." "szept." "okt." "nov." "dec.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("vasárnap" "hétfő" "kedd" "szerda" "csütörtök" "péntek" "szombat")
                       :SHORT-WEEKDAYS '("V" "H" "K" "Sze" "Cs" "P" "Szo")
                       :ERAS '("i.e." "i.u."))))

(setf (gethash :fr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janvier" "février" "mars" "avril" "mai" "juin" "juillet" "août" "septembre" "octobre" "novembre" "décembre")
                       :SHORT-MONTHS '("janv." "févr." "mars" "avr." "mai" "juin" "juil." "août" "sept." "oct." "nov." "déc.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("dimanche" "lundi" "mardi" "mercredi" "jeudi" "vendredi" "samedi")
                       :SHORT-WEEKDAYS '("dim." "lun." "mar." "mer." "jeu." "ven." "sam.")
                       :ERAS '("BC" "ap. J.-C."))))

(setf (gethash :en-ie *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :uk-ua *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "UAH")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("січня" "лютого" "березня" "квітня" "травня" "червня" "липня" "серпня" "вересня" "жовтня" "листопада" "грудня")
                       :SHORT-MONTHS '("січ" "лют" "бер" "квіт" "трав" "черв" "лип" "серп" "вер" "жовт" "лист" "груд")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("неділя" "понеділок" "вівторок" "середа" "четвер" "п'ятниця" "субота")
                       :SHORT-WEEKDAYS '("нд" "пн" "вт" "ср" "чт" "пт" "сб")
                       :ERAS '("до н.е." "після н.е."))))

(setf (gethash :pl-pl *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "PLN")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("styczeń" "luty" "marzec" "kwiecień" "maj" "czerwiec" "lipiec" "sierpień" "wrzesień" "październik" "listopad" "grudzień")
                       :SHORT-MONTHS '("sty" "lut" "mar" "kwi" "maj" "cze" "lip" "sie" "wrz" "paź" "lis" "gru")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("niedziela" "poniedziałek" "wtorek" "środa" "czwartek" "piątek" "sobota")
                       :SHORT-WEEKDAYS '("N" "Pn" "Wt" "Śr" "Cz" "Pt" "So")
                       :ERAS '("p.n.e." "n.e."))))

(setf (gethash :fr-lu *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janvier" "février" "mars" "avril" "mai" "juin" "juillet" "août" "septembre" "octobre" "novembre" "décembre")
                       :SHORT-MONTHS '("janv." "févr." "mars" "avr." "mai" "juin" "juil." "août" "sept." "oct." "nov." "déc.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("dimanche" "lundi" "mardi" "mercredi" "jeudi" "vendredi" "samedi")
                       :SHORT-WEEKDAYS '("dim." "lun." "mar." "mer." "jeu." "ven." "sam.")
                       :ERAS '("BC" "ap. J.-C."))))

(setf (gethash :nl-be *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januari" "februari" "maart" "april" "mei" "juni" "juli" "augustus" "september" "oktober" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mrt" "apr" "mei" "jun" "jul" "aug" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("zondag" "maandag" "dinsdag" "woensdag" "donderdag" "vrijdag" "zaterdag")
                       :SHORT-WEEKDAYS '("zo" "ma" "di" "wo" "do" "vr" "za")
                       :ERAS '("BC" "AD"))))

(setf (gethash :en-in *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "INR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ca-es *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("gener" "febrer" "març" "abril" "maig" "juny" "juliol" "agost" "setembre" "octubre" "novembre" "desembre")
                       :SHORT-MONTHS '("gen." "feb." "març" "abr." "maig" "juny" "jul." "ag." "set." "oct." "nov." "des.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("diumenge" "dilluns" "dimarts" "dimecres" "dijous" "divendres" "dissabte")
                       :SHORT-WEEKDAYS '("dg." "dl." "dt." "dc." "dj." "dv." "ds.")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ar-ma *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "MAD")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :es-bo *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "BOB")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :en-au *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "AUD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :sr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("јануар" "фебруар" "март" "април" "мај" "јун" "јул" "август" "септембар" "октобар" "новембар" "децембар")
                       :SHORT-MONTHS '("јан" "феб" "мар" "апр" "мај" "јун" "јул" "авг" "сеп" "окт" "нов" "дец")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("недеља" "понедељак" "уторак" "среда" "четвртак" "петак" "субота")
                       :SHORT-WEEKDAYS '("нед" "пон" "уто" "сре" "чет" "пет" "суб")
                       :ERAS '("п. н. е." "н. е"))))

(setf (gethash :zh-sg *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "SGD")
        :DATE-FORMAT (list
                       :AMPM '("上午" "下午")
                       :MONTHS '("一月" "二月" "三月" "四月" "五月" "六月" "七月" "八月" "九月" "十月" "十一月" "十二月")
                       :SHORT-MONTHS '("一月" "二月" "三月" "四月" "五月" "六月" "七月" "八月" "九月" "十月" "十一月" "十二月")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("星期日" "星期一" "星期二" "星期三" "星期四" "星期五" "星期六")
                       :SHORT-WEEKDAYS '("周日" "周一" "周二" "周三" "周四" "周五" "周六")
                       :ERAS '("公元前" "公元"))))

(setf (gethash :pt *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Janeiro" "Fevereiro" "Março" "Abril" "Maio" "Junho" "Julho" "Agosto" "Setembro" "Outubro" "Novembro" "Dezembro")
                       :SHORT-MONTHS '("Jan" "Fev" "Mar" "Abr" "Mai" "Jun" "Jul" "Ago" "Set" "Out" "Nov" "Dez")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Domingo" "Segunda-feira" "Terça-feira" "Quarta-feira" "Quinta-feira" "Sexta-feira" "Sábado")
                       :SHORT-WEEKDAYS '("Dom" "Seg" "Ter" "Qua" "Qui" "Sex" "Sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :uk *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("січня" "лютого" "березня" "квітня" "травня" "червня" "липня" "серпня" "вересня" "жовтня" "листопада" "грудня")
                       :SHORT-MONTHS '("січ" "лют" "бер" "квіт" "трав" "черв" "лип" "серп" "вер" "жовт" "лист" "груд")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("неділя" "понеділок" "вівторок" "середа" "четвер" "п'ятниця" "субота")
                       :SHORT-WEEKDAYS '("нд" "пн" "вт" "ср" "чт" "пт" "сб")
                       :ERAS '("до н.е." "після н.е."))))

(setf (gethash :es-sv *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "SVC")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ru-ru *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "RUB")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Январь" "Февраль" "Март" "Апрель" "Май" "Июнь" "Июль" "Август" "Сентябрь" "Октябрь" "Ноябрь" "Декабрь")
                       :SHORT-MONTHS '("янв" "фев" "мар" "апр" "май" "июн" "июл" "авг" "сен" "окт" "ноя" "дек")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("воскресенье" "понедельник" "вторник" "среда" "четверг" "пятница" "суббота")
                       :SHORT-WEEKDAYS '("Вс" "Пн" "Вт" "Ср" "Чт" "Пт" "Сб")
                       :ERAS '("до н.э." "н.э."))))

(setf (gethash :ko-kr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "KRW")
        :DATE-FORMAT (list
                       :AMPM '("오전" "오후")
                       :MONTHS '("1월" "2월" "3월" "4월" "5월" "6월" "7월" "8월" "9월" "10월" "11월" "12월")
                       :SHORT-MONTHS '("1월" "2월" "3월" "4월" "5월" "6월" "7월" "8월" "9월" "10월" "11월" "12월")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("일요일" "월요일" "화요일" "수요일" "목요일" "금요일" "토요일")
                       :SHORT-WEEKDAYS '("일" "월" "화" "수" "목" "금" "토")
                       :ERAS '("BC" "AD"))))

(setf (gethash :vi *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("SA" "CH")
                       :MONTHS '("tháng một" "tháng hai" "tháng ba" "tháng tư" "tháng năm" "tháng sáu" "tháng bảy" "tháng tám" "tháng chín" "tháng mười" "tháng mười một" "tháng mười hai")
                       :SHORT-MONTHS '("thg 1" "thg 2" "thg 3" "thg 4" "thg 5" "thg 6" "thg 7" "thg 8" "thg 9" "thg 10" "thg 11" "thg 12")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Chủ nhật" "Thứ hai" "Thứ ba" "Thứ tư" "Thứ năm" "Thứ sáu" "Thứ bảy")
                       :SHORT-WEEKDAYS '("CN" "Th 2" "Th 3" "Th 4" "Th 5" "Th 6" "Th 7")
                       :ERAS '("tr. CN" "sau CN"))))

(setf (gethash :ar-dz *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "DZD")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :vi-vn *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "VND")
        :DATE-FORMAT (list
                       :AMPM '("SA" "CH")
                       :MONTHS '("tháng một" "tháng hai" "tháng ba" "tháng tư" "tháng năm" "tháng sáu" "tháng bảy" "tháng tám" "tháng chín" "tháng mười" "tháng mười một" "tháng mười hai")
                       :SHORT-MONTHS '("thg 1" "thg 2" "thg 3" "thg 4" "thg 5" "thg 6" "thg 7" "thg 8" "thg 9" "thg 10" "thg 11" "thg 12")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Chủ nhật" "Thứ hai" "Thứ ba" "Thứ tư" "Thứ năm" "Thứ sáu" "Thứ bảy")
                       :SHORT-WEEKDAYS '("CN" "Th 2" "Th 3" "Th 4" "Th 5" "Th 6" "Th 7")
                       :ERAS '("tr. CN" "sau CN"))))

(setf (gethash :sr-me *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("јануар" "фебруар" "март" "април" "мај" "јун" "јул" "август" "септембар" "октобар" "новембар" "децембар")
                       :SHORT-MONTHS '("јан" "феб" "мар" "апр" "мај" "јун" "јул" "авг" "сеп" "окт" "нов" "дец")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("недеља" "понедељак" "уторак" "среда" "четвртак" "петак" "субота")
                       :SHORT-WEEKDAYS '("нед" "пон" "уто" "сре" "чет" "пет" "суб")
                       :ERAS '("п. н. е." "н. е"))))

(setf (gethash :sq *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("PD" "MD")
                       :MONTHS '("janar" "shkurt" "mars" "prill" "maj" "qershor" "korrik" "gusht" "shtator" "tetor" "nëntor" "dhjetor")
                       :SHORT-MONTHS '("Jan" "Shk" "Mar" "Pri" "Maj" "Qer" "Kor" "Gsh" "Sht" "Tet" "Nën" "Dhj")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("e diel" "e hënë" "e martë" "e mërkurë" "e enjte" "e premte" "e shtunë")
                       :SHORT-WEEKDAYS '("Die" "Hën" "Mar" "Mër" "Enj" "Pre" "Sht")
                       :ERAS '("p.e.r." "n.e.r."))))

(setf (gethash :ar-ly *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "LYD")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :ar *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :zh-cn *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "CNY")
        :DATE-FORMAT (list
                       :AMPM '("上午" "下午")
                       :MONTHS '("一月" "二月" "三月" "四月" "五月" "六月" "七月" "八月" "九月" "十月" "十一月" "十二月")
                       :SHORT-MONTHS '("一月" "二月" "三月" "四月" "五月" "六月" "七月" "八月" "九月" "十月" "十一月" "十二月")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("星期日" "星期一" "星期二" "星期三" "星期四" "星期五" "星期六")
                       :SHORT-WEEKDAYS '("星期日" "星期一" "星期二" "星期三" "星期四" "星期五" "星期六")
                       :ERAS '("公元前" "公元"))))

(setf (gethash :be-by *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "BYR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("студзеня" "лютага" "сакавіка" "красавіка" "мая" "чрвеня" "ліпеня" "жніўня" "верасня" "кастрычніка" "листапада" "снежня")
                       :SHORT-MONTHS '("стд" "лют" "скв" "крс" "май" "чрв" "лпн" "жнв" "врс" "кст" "лст" "снж")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("нядзеля" "панядзелак" "аўторак" "серада" "чацвер" "пятніца" "субота")
                       :SHORT-WEEKDAYS '("нд" "пн" "ат" "ср" "чц" "пт" "сб")
                       :ERAS '("да н.е." "н.е."))))

(setf (gethash :zh-hk *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "HKD")
        :DATE-FORMAT (list
                       :AMPM '("上午" "下午")
                       :MONTHS '("一月" "二月" "三月" "四月" "五月" "六月" "七月" "八月" "九月" "十月" "十一月" "十二月")
                       :SHORT-MONTHS '("1月" "2月" "3月" "4月" "5月" "6月" "7月" "8月" "9月" "10月" "11月" "12月")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("星期日" "星期一" "星期二" "星期三" "星期四" "星期五" "星期六")
                       :SHORT-WEEKDAYS '("日" "一" "二" "三" "四" "五" "六")
                       :ERAS '("西元前" "西元"))))

(setf (gethash :ja *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("午前" "午後")
                       :MONTHS '("1月" "2月" "3月" "4月" "5月" "6月" "7月" "8月" "9月" "10月" "11月" "12月")
                       :SHORT-MONTHS '("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("日曜日" "月曜日" "火曜日" "水曜日" "木曜日" "金曜日" "土曜日")
                       :SHORT-WEEKDAYS '("日" "月" "火" "水" "木" "金" "土")
                       :ERAS '("紀元前" "西暦"))))

(setf (gethash :iw-il *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "ILS")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("ינואר" "פברואר" "מרץ" "אפריל" "מאי" "יוני" "יולי" "אוגוסט" "ספטמבר" "אוקטובר" "נובמבר" "דצמבר")
                       :SHORT-MONTHS '("ינו" "פבר" "מרץ" "אפר" "מאי" "יונ" "יול" "אוג" "ספט" "אוק" "נוב" "דצמ")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("יום ראשון" "יום שני" "יום שלישי" "יום רביעי" "יום חמישי" "יום שישי" "שבת")
                       :SHORT-WEEKDAYS '("א" "ב" "ג" "ד" "ה" "ו" "ש")
                       :ERAS '("לסה"נ" "לפסה"נ"))))

(setf (gethash :bg-bg *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "BGN")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Януари" "Февруари" "Март" "Април" "Май" "Юни" "Юли" "Август" "Септември" "Октомври" "Ноември" "Декември")
                       :SHORT-MONTHS '("I" "II" "III" "IV" "V" "VI" "VII" "VIII" "IX" "X" "XI" "XII")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Неделя" "Понеделник" "Вторник" "Сряда" "Четвъртък" "Петък" "Събота")
                       :SHORT-WEEKDAYS '("Нд" "Пн" "Вт" "Ср" "Чт" "Пт" "Сб")
                       :ERAS '("пр.н.е." "н.е."))))

(setf (gethash :in *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Januari" "Februari" "Maret" "April" "Mei" "Juni" "Juli" "Agustus" "September" "Oktober" "November" "Desember")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "Mei" "Jun" "Jul" "Agu" "Sep" "Okt" "Nov" "Des")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Minggu" "Senin" "Selasa" "Rabu" "Kamis" "Jumat" "Sabtu")
                       :SHORT-WEEKDAYS '("Min" "Sen" "Sel" "Rab" "Kam" "Jum" "Sab")
                       :ERAS '("BCE" "CE"))))

(setf (gethash :mt-mt *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "MTL")
        :DATE-FORMAT (list
                       :AMPM '("QN" "WN")
                       :MONTHS '("Jannar" "Frar" "Marzu" "April" "Mejju" "Ġunju" "Lulju" "Awissu" "Settembru" "Ottubru" "Novembru" "Diċembru")
                       :SHORT-MONTHS '("Jan" "Fra" "Mar" "Apr" "Mej" "Ġun" "Lul" "Awi" "Set" "Ott" "Nov" "Diċ")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Il-Ħadd" "It-Tnejn" "It-Tlieta" "L-Erbgħa" "Il-Ħamis" "Il-Ġimgħa" "Is-Sibt")
                       :SHORT-WEEKDAYS '("Ħad" "Tne" "Tli" "Erb" "Ħam" "Ġim" "Sib")
                       :ERAS '("QK" "WK"))))

(setf (gethash :es-py *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "PYG")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :sl *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januar" "februar" "marec" "april" "maj" "junij" "julij" "avgust" "september" "oktober" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "maj" "jun" "jul" "avg" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Nedelja" "Ponedeljek" "Torek" "Sreda" "Četrtek" "Petek" "Sobota")
                       :SHORT-WEEKDAYS '("Ned" "Pon" "Tor" "Sre" "Čet" "Pet" "Sob")
                       :ERAS '("pr.n.š." "po Kr."))))

(setf (gethash :fr-fr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("janvier" "février" "mars" "avril" "mai" "juin" "juillet" "août" "septembre" "octobre" "novembre" "décembre")
                       :SHORT-MONTHS '("janv." "févr." "mars" "avr." "mai" "juin" "juil." "août" "sept." "oct." "nov." "déc.")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("dimanche" "lundi" "mardi" "mercredi" "jeudi" "vendredi" "samedi")
                       :SHORT-WEEKDAYS '("dim." "lun." "mar." "mer." "jeu." "ven." "sam.")
                       :ERAS '("BC" "ap. J.-C."))))

(setf (gethash :cs-cz *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "CZK")
        :DATE-FORMAT (list
                       :AMPM '("dop." "odp.")
                       :MONTHS '("leden" "únor" "březen" "duben" "květen" "červen" "červenec" "srpen" "září" "říjen" "listopad" "prosinec")
                       :SHORT-MONTHS '("I" "II" "III" "IV" "V" "VI" "VII" "VIII" "IX" "X" "XI" "XII")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Neděle" "Pondělí" "Úterý" "Středa" "Čtvrtek" "Pátek" "Sobota")
                       :SHORT-WEEKDAYS '("Ne" "Po" "Út" "St" "Čt" "Pá" "So")
                       :ERAS '("př.Kr." "po Kr."))))

(setf (gethash :it-ch *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\' :DECIMAL-SEPARATOR #\. "CHF")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("gennaio" "febbraio" "marzo" "aprile" "maggio" "giugno" "luglio" "agosto" "settembre" "ottobre" "novembre" "dicembre")
                       :SHORT-MONTHS '("gen" "feb" "mar" "apr" "mag" "giu" "lug" "ago" "set" "ott" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domenica" "lunedì" "martedì" "mercoledì" "giovedì" "venerdì" "sabato")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mer" "gio" "ven" "sab")
                       :ERAS '("BC" "dopo Cristo"))))

(setf (gethash :ro-ro *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "RON")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("ianuarie" "februarie" "martie" "aprilie" "mai" "iunie" "iulie" "august" "septembrie" "octombrie" "noiembrie" "decembrie")
                       :SHORT-MONTHS '("Ian" "Feb" "Mar" "Apr" "Mai" "Iun" "Iul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("duminică" "luni" "marţi" "miercuri" "joi" "vineri" "sîmbătă")
                       :SHORT-WEEKDAYS '("D" "L" "Ma" "Mi" "J" "V" "S")
                       :ERAS '("d.C." "î.d.C."))))

(setf (gethash :es-pr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "USD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :en-ca *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "CAD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :de-de *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Januar" "Februar" "März" "April" "Mai" "Juni" "Juli" "August" "September" "Oktober" "November" "Dezember")
                       :SHORT-MONTHS '("Jan" "Feb" "Mrz" "Apr" "Mai" "Jun" "Jul" "Aug" "Sep" "Okt" "Nov" "Dez")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Sonntag" "Montag" "Dienstag" "Mittwoch" "Donnerstag" "Freitag" "Samstag")
                       :SHORT-WEEKDAYS '("So" "Mo" "Di" "Mi" "Do" "Fr" "Sa")
                       :ERAS '("v. Chr." "n. Chr."))))

(setf (gethash :ga *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("a.m." "p.m.")
                       :MONTHS '("Eanáir" "Feabhra" "Márta" "Aibreán" "Bealtaine" "Meitheamh" "Iúil" "Lúnasa" "Meán Fómhair" "Deireadh Fómhair" "Samhain" "Nollaig")
                       :SHORT-MONTHS '("Ean" "Feabh" "Márta" "Aib" "Beal" "Meith" "Iúil" "Lún" "MFómh" "DFómh" "Samh" "Noll")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Dé Domhnaigh" "Dé Luain" "Dé Máirt" "Dé Céadaoin" "Déardaoin" "Dé hAoine" "Dé Sathairn")
                       :SHORT-WEEKDAYS '("Domh" "Luan" "Máirt" "Céad" "Déar" "Aoine" "Sath")
                       :ERAS '("RC" "AD"))))

(setf (gethash :de-lu *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "EUR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Januar" "Februar" "März" "April" "Mai" "Juni" "Juli" "August" "September" "Oktober" "November" "Dezember")
                       :SHORT-MONTHS '("Jan" "Feb" "Mrz" "Apr" "Mai" "Jun" "Jul" "Aug" "Sep" "Okt" "Nov" "Dez")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Sonntag" "Montag" "Dienstag" "Mittwoch" "Donnerstag" "Freitag" "Samstag")
                       :SHORT-WEEKDAYS '("So" "Mo" "Di" "Mi" "Do" "Fr" "Sa")
                       :ERAS '("v. Chr." "n. Chr."))))

(setf (gethash :de *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Januar" "Februar" "März" "April" "Mai" "Juni" "Juli" "August" "September" "Oktober" "November" "Dezember")
                       :SHORT-MONTHS '("Jan" "Feb" "Mrz" "Apr" "Mai" "Jun" "Jul" "Aug" "Sep" "Okt" "Nov" "Dez")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Sonntag" "Montag" "Dienstag" "Mittwoch" "Donnerstag" "Freitag" "Samstag")
                       :SHORT-WEEKDAYS '("So" "Mo" "Di" "Mi" "Do" "Fr" "Sa")
                       :ERAS '("v. Chr." "n. Chr."))))

(setf (gethash :es-ar *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "ARS")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

(setf (gethash :sk *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("január" "február" "marec" "apríl" "máj" "jún" "júl" "august" "september" "október" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "máj" "jún" "júl" "aug" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Nedeľa" "Pondelok" "Utorok" "Streda" "Štvrtok" "Piatok" "Sobota")
                       :SHORT-WEEKDAYS '("Ne" "Po" "Ut" "St" "Št" "Pi" "So")
                       :ERAS '("pred n.l." "n.l."))))

(setf (gethash :ms-my *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "MYR")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Januari" "Februari" "Mac" "April" "Mei" "Jun" "Julai" "Ogos" "September" "Oktober" "November" "Disember")
                       :SHORT-MONTHS '("Jan" "Feb" "Mac" "Apr" "Mei" "Jun" "Jul" "Ogos" "Sep" "Okt" "Nov" "Dis")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Ahad" "Isnin" "Selasa" "Rabu" "Khamis" "Jumaat" "Sabtu")
                       :SHORT-WEEKDAYS '("Ahd" "Isn" "Sel" "Rab" "Kha" "Jum" "Sab")
                       :ERAS '("BCE" "CE"))))

(setf (gethash :hr-hr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "HRK")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("siječanj" "veljača" "ožujak" "travanj" "svibanj" "lipanj" "srpanj" "kolovoz" "rujan" "listopad" "studeni" "prosinac")
                       :SHORT-MONTHS '("sij" "vel" "ožu" "tra" "svi" "lip" "srp" "kol" "ruj" "lis" "stu" "pro")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("nedjelja" "ponedjeljak" "utorak" "srijeda" "četvrtak" "petak" "subota")
                       :SHORT-WEEKDAYS '("ned" "pon" "uto" "sri" "čet" "pet" "sub")
                       :ERAS '("BC" "AD"))))

(setf (gethash :en-gs *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "SGD")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("January" "February" "March" "April" "May" "June" "July" "August" "September" "October" "November" "December")
                       :SHORT-MONTHS '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday")
                       :SHORT-WEEKDAYS '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
                       :ERAS '("BC" "AD"))))

(setf (gethash :da *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januar" "februar" "marts" "april" "maj" "juni" "juli" "august" "september" "oktober" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "maj" "jun" "jul" "aug" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("søndag" "mandag" "tirsdag" "onsdag" "torsdag" "fredag" "lørdag")
                       :SHORT-WEEKDAYS '("sø" "ma" "ti" "on" "to" "fr" "lø")
                       :ERAS '("BC" "AD"))))

(setf (gethash :mt *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("QN" "WN")
                       :MONTHS '("Jannar" "Frar" "Marzu" "April" "Mejju" "Ġunju" "Lulju" "Awissu" "Settembru" "Ottubru" "Novembru" "Diċembru")
                       :SHORT-MONTHS '("Jan" "Fra" "Mar" "Apr" "Mej" "Ġun" "Lul" "Awi" "Set" "Ott" "Nov" "Diċ")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Il-Ħadd" "It-Tnejn" "It-Tlieta" "L-Erbgħa" "Il-Ħamis" "Il-Ġimgħa" "Is-Sibt")
                       :SHORT-WEEKDAYS '("Ħad" "Tne" "Tli" "Erb" "Ħam" "Ġim" "Sib")
                       :ERAS '("QK" "WK"))))

(setf (gethash :pl *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("styczeń" "luty" "marzec" "kwiecień" "maj" "czerwiec" "lipiec" "sierpień" "wrzesień" "październik" "listopad" "grudzień")
                       :SHORT-MONTHS '("sty" "lut" "mar" "kwi" "maj" "cze" "lip" "sie" "wrz" "paź" "lis" "gru")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("niedziela" "poniedziałek" "wtorek" "środa" "czwartek" "piątek" "sobota")
                       :SHORT-WEEKDAYS '("N" "Pn" "Wt" "Śr" "Cz" "Pt" "So")
                       :ERAS '("p.n.e." "n.e."))))

(setf (gethash :ar-om *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "OMR")
        :DATE-FORMAT (list
                       :AMPM '("ص" "م")
                       :MONTHS '("يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر")
                       :SHORT-MONTHS '("ينا" "فبر" "مار" "أبر" "ماي" "يون" "يول" "أغس" "سبت" "أكت" "نوف" "ديس")
                       :FIRST-DAY-OF-THE-WEEK 7
                       :WEEKDAYS '("الأحد" "الاثنين" "الثلاثاء" "الأربعاء" "الخميس" "الجمعة" "السبت")
                       :SHORT-WEEKDAYS '("ح" "ن" "ث" "ر" "خ" "ج" "س")
                       :ERAS '("ق.م" "م"))))

(setf (gethash :tr *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Ocak" "Şubat" "Mart" "Nisan" "Mayıs" "Haziran" "Temmuz" "Ağustos" "Eylül" "Ekim" "Kasım" "Aralık")
                       :SHORT-MONTHS '("Oca" "Şub" "Mar" "Nis" "May" "Haz" "Tem" "Ağu" "Eyl" "Eki" "Kas" "Ara")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Pazar" "Pazartesi" "Salı" "Çarşamba" "Perşembe" "Cuma" "Cumartesi")
                       :SHORT-WEEKDAYS '("Paz" "Pzt" "Sal" "Çar" "Per" "Cum" "Cmt")
                       :ERAS '("BC" "AD"))))

(setf (gethash :th-th *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "THB")
        :DATE-FORMAT (list
                       :AMPM '("ก่อนเที่ยง" "หลังเที่ยง")
                       :MONTHS '("มกราคม" "กุมภาพันธ์" "มีนาคม" "เมษายน" "พฤษภาคม" "มิถุนายน" "กรกฎาคม" "สิงหาคม" "กันยายน" "ตุลาคม" "พฤศจิกายน" "ธันวาคม")
                       :SHORT-MONTHS '("ม.ค." "ก.พ." "มี.ค." "เม.ย." "พ.ค." "มิ.ย." "ก.ค." "ส.ค." "ก.ย." "ต.ค." "พ.ย." "ธ.ค.")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("วันอาทิตย์" "วันจันทร์" "วันอังคาร" "วันพุธ" "วันพฤหัสบดี" "วันศุกร์" "วันเสาร์")
                       :SHORT-WEEKDAYS '("อา." "จ." "อ." "พ." "พฤ." "ศ." "ส.")
                       :ERAS '("ปีก่อนคริสต์กาลที่" "ค.ศ."))))

(setf (gethash :el *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "XXX")
        :DATE-FORMAT (list
                       :AMPM '("πμ" "μμ")
                       :MONTHS '("Ιανουάριος" "Φεβρουάριος" "Μάρτιος" "Απρίλιος" "Μάϊος" "Ιούνιος" "Ιούλιος" "Αύγουστος" "Σεπτέμβριος" "Οκτώβριος" "Νοέμβριος" "Δεκέμβριος")
                       :SHORT-MONTHS '("Ιαν" "Φεβ" "Μαρ" "Απρ" "Μαϊ" "Ιουν" "Ιουλ" "Αυγ" "Σεπ" "Οκτ" "Νοε" "Δεκ")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("Κυριακή" "Δευτέρα" "Τρίτη" "Τετάρτη" "Πέμπτη" "Παρασκευή" "Σάββατο")
                       :SHORT-WEEKDAYS '("Κυρ" "Δευ" "Τρι" "Τετ" "Πεμ" "Παρ" "Σαβ")
                       :ERAS '("BC" "AD"))))

(setf (gethash :ms *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "XXX")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("Januari" "Februari" "Mac" "April" "Mei" "Jun" "Julai" "Ogos" "September" "Oktober" "November" "Disember")
                       :SHORT-MONTHS '("Jan" "Feb" "Mac" "Apr" "Mei" "Jun" "Jul" "Ogos" "Sep" "Okt" "Nov" "Dis")
                       :FIRST-DAY-OF-THE-WEEK 1
                       :WEEKDAYS '("Ahad" "Isnin" "Selasa" "Rabu" "Khamis" "Jumaat" "Sabtu")
                       :SHORT-WEEKDAYS '("Ahd" "Isn" "Sel" "Rab" "Kha" "Jum" "Sab")
                       :ERAS '("BCE" "CE"))))

(setf (gethash :sv-se *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\  :DECIMAL-SEPARATOR #\, "SEK")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januari" "februari" "mars" "april" "maj" "juni" "juli" "augusti" "september" "oktober" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "maj" "jun" "jul" "aug" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("söndag" "måndag" "tisdag" "onsdag" "torsdag" "fredag" "lördag")
                       :SHORT-WEEKDAYS '("sö" "må" "ti" "on" "to" "fr" "lö")
                       :ERAS '("BC" "AD"))))

(setf (gethash :da-dk *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\. :DECIMAL-SEPARATOR #\, "DKK")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("januar" "februar" "marts" "april" "maj" "juni" "juli" "august" "september" "oktober" "november" "december")
                       :SHORT-MONTHS '("jan" "feb" "mar" "apr" "maj" "jun" "jul" "aug" "sep" "okt" "nov" "dec")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("søndag" "mandag" "tirsdag" "onsdag" "torsdag" "fredag" "lørdag")
                       :SHORT-WEEKDAYS '("sø" "ma" "ti" "on" "to" "fr" "lø")
                       :ERAS '("BC" "AD"))))

(setf (gethash :es-hn *locales*)
      (list 
        :NUMBER-FORMAT (list :GROUPING-SEPARATOR #\, :DECIMAL-SEPARATOR #\. "HNL")
        :DATE-FORMAT (list
                       :AMPM '("AM" "PM")
                       :MONTHS '("enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre")
                       :SHORT-MONTHS '("ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sep" "oct" "nov" "dic")
                       :FIRST-DAY-OF-THE-WEEK 2
                       :WEEKDAYS '("domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado")
                       :SHORT-WEEKDAYS '("dom" "lun" "mar" "mié" "jue" "vie" "sáb")
                       :ERAS '("BC" "AD"))))

