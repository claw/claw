;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; $Header: src/components.lisp $

;;; Copyright (c) 2008, Andrea Chiumenti.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :claw-i18n)

(defvar *current-locale* :en
  "Default locale for claw-i18n package")

(defvar *current-dispatcher* nil
  "Default dispatcher into the system")

(defvar *locales* (make-hash-table :test 'equal)
  "A hash table of locale key strings and lists of locale directives.
You should use locale access functions to get its internal values.")

(defun number-format-grouping-separator (&optional (locale *current-locale*))
  "Returns the character used as thousands grouping separator for numbers"
  (getf (getf (gethash locale *locales*) :number-format) :grouping-separator))

(defun number-format-decimal-separator (&optional (locale *current-locale*))
  "Returns the character used as decimals separator for numbers"
  (getf (getf (gethash locale *locales*) :number-format) :decimal-separator))

(defun ampm (&optional (locale *current-locale*))
  "Returns a list with the localized version of AM and PM for time"
  (getf (gethash locale *locales*) :ampm))

(defun monthes (&optional (locale *current-locale*))
  "Returns a localized list of monthes in long form"
  (getf (gethash locale *locales*) :months))

(defun short-monthes (&optional (locale *current-locale*))
  "Returns a localized list of monthes in short form"
  (getf (gethash locale *locales*) :short-months))

(defun first-day-of-the-week (&optional (locale *current-locale*))
  "Returns the first day position of the week for the given locale, being sunday on position 0 and saturday on position 6"
  (1- (getf (gethash locale *locales*) :first-day-of-the-week)))

(defun weekdays (&optional (locale *current-locale*))
  "Returns a localized list of days of the week in long form"
  (getf (gethash locale *locales*) :weekdays))

(defun short-weekdays (&optional (locale *current-locale*))
  "Returns a localized list of days of the week in short form"
  (getf (gethash locale *locales*) :short-weekdays))

(defun eras (&optional (locale *current-locale*))
  "Returns a list with the localized version of BC and AD eras"
  (getf (gethash locale *locales*) :eras))

;;-----------------------------------------------------------------------------------------------------------

(defgeneric message-dispatch (object key locale)
  (:documentation "Returns the KEY translation by the given LOCALE"))

(defclass message-dispatcher ()
  ((message-dispatcher-locales :initarg :locales
                               :accessor message-dispatcher-locales
                               :documentation "List of locales accepted by this dispatcher"))
  (:default-initargs :locales nil)
  (:documentation "This is and interface for message dispatchers"))

(defmethod message-dispatch ((message-dispatcher message-dispatcher) key locale) nil)

(defgeneric simple-message-dispatcher-add-message (simple-message-dispatcher 
                                                  locale key value)
  (:documentation "Adds a localized message to a SIMPLE-MESSAGE-DISPATCHER instance"))


(defclass simple-message-dispatcher (message-dispatcher)
  ((locales :initform (make-hash-table :test #'equal)
            :accessor simple-message-dispatcher-locales
            :documentation "Hash table of locales strings and KEY/VALUE message pairs"))
  (:documentation "A message disptcher that leave data unchanged during encoding and decoding phases."))

(defmethod simple-message-dispatcher-add-message ((obj simple-message-dispatcher) 
                                                  locale key value)
  (let ((locales (message-dispatcher-locales obj))
        (messages (or (gethash locale (simple-message-dispatcher-locales obj))
                      (setf (gethash locale (simple-message-dispatcher-locales obj)) 
                            (make-hash-table :test #'equal)))))
    (setf (message-dispatcher-locales obj) (pushnew locale locales))
    (setf (gethash key messages) value)))

(defmethod message-dispatch ((obj simple-message-dispatcher) key locale)
  (let ((messages (gethash locale (simple-message-dispatcher-locales obj))))
    (and messages (gethash key messages))))

(defun do-message (key &optional default (locale *current-locale*) (message-dispatcher *current-dispatcher*))
  "This function dispatches a message with the *claw-current-lisplet* message dispatcher object. Locale is 'very' optional."
  (or (and message-dispatcher
           (message-dispatch message-dispatcher key locale))
      default))

(defmacro with-message (key &optional default locale message-dispatcher)
  `(lambda () (do-message ,key ,default (or ,locale *current-locale*) (or ,message-dispatcher *current-dispatcher*))))

