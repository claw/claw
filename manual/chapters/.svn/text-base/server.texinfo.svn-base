@node Server
@comment  node-name,  next,  previous,  up
@chapter The server

@value{claw} wraps the Hunchentoot (see: @url{http://www.weitz.de/hunchentoot/, unchentoot}), a wonderful as powerful web server written in Common Lisp, 
into the @code{CLAWSERVER} class.

As an Hunchentoot wrapper @code{CLAWSERVER} ``provides facilities like automatic session handling (with and without cookies), logging 
(to Apache's log files or to a file in the file system), customizable error handling, and easy access to GET and POST parameters sent by the client.''

@section Understanding the clawserver

@code{CLAWSERVER} is not only a Hunchentoot wrapper, it is also the common place where you put your web applications 
built with @value{claw} into lisplet that you can see as application resource containers and request dispatchers.

@subsection @code{CLAWSERVER} instance initialization

When you want to instantiate a @code{CLAWSERVER} class, remember that it accepts the following initialization arguments:
@itemize @minus
@item
@emph{port} The port the server will be listening on, default is 80 
@item
@emph{sslport} The SSL port the server will be listening on, default is 443 if the server has a certificate file defined.
@item
@emph{address} A string denoting an IP address, if provided then the server only receives connections for that address.
If address is NIL, then the server will receive connections to all IP addresses on the machine (default).
@item
@emph{name} Should be a symbol which can be used to name the server. 
This name can utilized when defining easy handlers. The default name is an uninterned symbol as returned by GENSYM
@item
@emph{sslname} Should be a symbol which can be used to name the server running in SSL mode when a certificate file is provided. 
This name can utilized when defining easy handlers. The default name is an uninterned symbol as returned by GENSYM
@item
@emph{mod-lisp-p} If  true (the default is NIL), the server will act as a back-end for mod_lisp, otherwise it will be a stand-alone web server.
@item
@emph{use-apache-log-p} If true (which is the default), log messages will be written to the Apache log file - this parameter has no effect if @emph{mod-lisp-p} is NIL.
@item
@emph{input-chunking-p} If true (which is the default), the server will accept request bodies without a Content-Length header if the client uses chunked transfer encoding.
@item
@emph{read-timeout} Is the read timeout (in seconds) for the socket stream used by the server. 
The default value is @url{http://www.weitz.de/hunchentoot/#*default-read-timeout*,HUNCHENTOOT:*DEFAULT-READ-TIMEOUT*} (20 seconds)
@item
@emph{write-timeout} Is the write timeout (in seconds) for the socket stream used by the server. 
The default value is @url{http://www.weitz.de/hunchentoot/#*default-write-timeout*,HUNCHENTOOT:*DEFAULT-WRITE-TIMEOUT*} (20 seconds)
@item
@emph{setuid} On Unix systems, changes the UID of the process directly after the server has been started.
@item
@emph{setgid} On Unix systems, changes the GID of the process directly after the server has been started.
@item
@emph{ssl-certificate-file} If you want your server to use SSL, you must provide the pathname designator(s) for the certificate file (must be in PEM format).
@item
@emph{ssl-privatekey-file} the pathname designator(s) for the private key file (must be in PEM format).
@item
@emph{ssl-privatekey-password} If private key file needs a password set this parameter to the required password
@end itemize

@subsection @code{CLAWSERVER} class methods

@sp 1
@fnindex clawserver-port
@noindent
@code{clawserver-port obj}@*
@code{(setf clawserver-port) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The numeric value of listening port to assign 
@end itemize
Returns and sets the port on which the server is listening to (default 80).
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-sslport
@noindent
@code{clawserver-sslport obj}@*
@code{(setf clawserver-sslport) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The numeric value of listening port to assign for SSL connections
@end itemize
Returns and sets the port on which the server is listening to in SSL mode if a certificate file is provided (default 443).
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-address
@noindent
@code{clawserver-address obj}@*
@code{(setf clawserver-address) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The string value denoting the IP address
@end itemize
Returns and sets the IP address where the server is bound to (default @code{NIL} @result{} any).
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-name
@noindent
@code{clawserver-name obj}@*
@code{(setf clawserver-name) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The symbol value denoting the server name
@end itemize
Should be a symbol which can be used to name the server. 
This name can utilized when defining easy handlers. The default name is an uninterned symbol as returned by GENSYM

@sp 1
@fnindex clawserver-sslname
@noindent
@code{clawserver-sslname obj}@*
@code{(setf clawserver-sslname) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The symbol value denoting the server name running in SSL mode
@end itemize
Should be a symbol which can be used to name the server running in SSL mode, when a certificate file is provided. 
This name can utilized when defining easy handlers. The default name is an uninterned symbol as returned by GENSYM

@sp 1
@fnindex clawserver-mod-lisp-p
@noindent
@code{clawserver-mod-lisp-p obj}@*
@code{(setf clawserver-mod-lisp-p) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The boolean value denoting the use of mod_lisp.
@end itemize
Returns and sets the server startup modality .
If  true (the default is @code{NIL}), the server will act as a back-end for mod_lisp, otherwise it will be a stand-alone web server.
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-use-apache-log-p
@noindent
@code{clawserver-use-apache-log-p obj}@*
@code{(setf clawserver-use-apache-log-p) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The boolean value denoting the use of Apache log.
@end itemize
Returns and sets where the server should log messages. This parameter has no effects if clawserver-mod-lisp-p is set to @code{NIL}. (default @code{T} if @code{mod_lisp} 
is activated.
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-input-chunking-p
@noindent
@code{clawserver-input-chunking-p obj}@*
@code{(setf clawserver-input-chunking-p) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The boolean value denoting the ability to accept request bodies without a Content-Length header.
@end itemize
Returns and sets the ability to accept request bodies without a Content-Length header (default is @code{T})
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-read-timeout
@noindent
@code{clawserver-read-timeout obj}@*
@code{(setf clawserver-read-timeout) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The integer value denoting the server read timeout.
@end itemize
Returns and sets the server read timeout in seconds (default is @code{T})
(default to @url{http://www.weitz.de/hunchentoot/#*default-read-timeout*,HUNCHENTOOT:*DEFAULT-READ-TIMEOUT*} [20 seconds]).
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-write-timeout
@noindent
@code{clawserver-write-timeout obj}@*
@code{(setf clawserver-write-timeout) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The integer value denoting the server write timeout.
@end itemize
Returns and sets the server write timeout in seconds (default is @code{T})
(default to @url{http://www.weitz.de/hunchentoot/#*default-read-timeout*,HUNCHENTOOT:*DEFAULT-WRITE-TIMEOUT*} [20 seconds]).
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-setuid
@noindent
@code{clawserver-setuid obj}@*
@code{(setf clawserver-setuid) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The string or integer value of the UID with which the server instance will run.
@end itemize
Returns and sets the server instance UID (user id).
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-setgid
@noindent
@code{clawserver-setgid obj}@*
@code{(setf clawserver-setgid) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} The string or integer value of the GID with which the server instance will run.
@end itemize
Returns and sets the server instance GID (group id).
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-ssl-certificate-file
@noindent
@code{clawserver-ssl-certificate-file obj}@*
@code{(setf clawserver-ssl-certificate-file) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} Pathname designator(s) for the certificate file
@end itemize
Returns and sets the pathname designator(s) for the certificate file if the @code{CLAWSERVER} is SSL enabled
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-ssl-privatekey-file
@noindent
@code{clawserver-ssl-privatekey-file obj}@*
@code{(setf clawserver-ssl-privatekey-file) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} Pathname designator(s) for the private key file
@end itemize
Returns and sets the pathname designator(s) for the private key file if the @code{CLAWSERVER} is SSL enabled
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-ssl-privatekey-password
@noindent
@code{clawserver-ssl-privatekey-password obj}@*
@code{(setf clawserver-ssl-privatekey-password) val obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{val} Password for the private key file
@end itemize
Returns and sets the password for the private key file if the @code{CLAWSERVER} is SSL enabled
If the server is started and you try to change the listening value an error will be signaled

@sp 1
@fnindex clawserver-start
@noindent
@code{clawserver-start obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@end itemize
Make the @code{CLAWSERVER} begin to dispatch requests

@sp 1
@fnindex clawserver-stop
@noindent
@code{clawserver-stop obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@end itemize
Make the @code{CLAWSERVER} stop.

@sp 1
@fnindex clawserver-register-lisplet
@fnindex lisplet
@noindent
@code{clawserver-register-lisplet clawserver lisplet-obj}@*
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{lisplet-obj} A @code{LISPLET} class instance
@end itemize
Registers a @code{LISPLET}, that is an `application container` for request dispatching.

@sp 1
@fnindex clawserver-unregister-lisplet
@noindent
@code{clawserver-unregister-lisplet clawserver lisplet-obj}
@indent

@itemize
@item
@emph{obj} The @code{CLAWSERVER} instance
@item
@emph{lisplet-obj} A @code{LISPLET} class instance
@end itemize
Unregisters a @code{LISPLET}, that is an `application container`, an so all it's resources,  from the @code{CLAWSERVER} instance.

@section Starting the server

Starting @value{claw} is very easy and requires a minimal effort.
@value{claw} supports both http and https protocols, thought enabling SSL connection for @value{claw} requires 
a little more work then having it responding only to http calls.

@subsection Making @value{claw} work on http protocol

To simply start @value{claw} server, without enabling SSL requests handling, you just need few steps:

@cartouche
@lisp
(defparameter *clawserver* (make-instance 'clawserver))
(clawserver-start *clawserver*)
@end lisp
@end cartouche

This will start the web server on port 80 that is the default.


Of course you can create a parametrized version of @code{CLAWSERVER} instance for example specifying the listening port as the following:

@cartouche
@lisp
(defparameter *clawserver* (make-instance 'clawserver :port 4242))
(clawserver-start *clawserver*)
@end lisp
@end cartouche

@subsection Making @value{claw} work on both http and https protocols

To enable @value{claw} to https firt you need a certificate file.
A quick way to get one on a Linux system is to use openssl to generate the a certificate PEM file, the following example explains how to do.

Firstly you'll generate the private key file:

@cartouche
@example
#> openssl genrsa -out privkey.pem 2048
@sp 1
Generating RSA private key, 2048 bit long modulus
............................+++
..................................................+++
e is 65537 (0x10001)
@sp 1
#>
@end example
@end cartouche

Then the certificate file:

@cartouche
@example
#> openssl req -new -x509 -key privkey.pem -out cacert.pem -days 1095
@sp 1
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:IT
State or Province Name (full name) [Some-State]: bla-bla
Locality Name (eg, city) []: bla-bla
Organization Name (eg, company) [Internet Widgits Pty Ltd]: mycompany
Organizational Unit Name (eg, section) []:
Common Name (eg, YOUR name) []:www.mycompany.com
Email Address []:admin@@mycompany.com
@sp 1
#>
@end example
@end cartouche

Now you can start @code{CLAWSERVER} in both http and https mode:

@cartouche
@lisp
(defparameter *clawserver* (make-instance 'clawserver :port 4242 
              :sslport 4443 
              :ssl-certificate-file #P"/path/to/certificate/cacert.pem" 
              :ssl-privatekey-file #P"/path/to/certificate/privkey.pem")))
(clawserver-start *clawserver*)
@end lisp
@end cartouche

@value{claw} is now up and you can browse it with your browser using address http://www.yourcompany.com:4242 and http://www.yourcompany.com:4443. 
Of course you will have only a 404 response page!

@subsection Making all applications to work under a common path

You have the possibility to define a common path to mapp all @value{claw} applications registered into the server,
defining the global variable @code{*CLAWSERVER-BASE-PATH*}. This way, if you have two applcations mapped for example to
``/applicationA'' and ``/applicationB'', setting that variable to the common path ``/yourcompany'' with the instruction
@cartouche
@lisp
(setf *clawserver-base-path* "/yourcompany")
@end lisp
@end cartouche
you will have the two applications now mapped to ``/yourcompany/applicationA'' and ``/yourcompany/applicationB''.
