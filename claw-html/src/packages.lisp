;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; $Header: src/package.lisp $

;;; Copyright (c) 2008, Andrea Chiumenti.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :cl-user)


(defpackage :claw-html
  (:use :cl 
        ;:closer-mop 
        :claw-as
        :local-time 
        :parenscript 
        :cl-ppcre 
        :split-sequence 
        :parenscript)
  (:shadow :flatten)
  (:shadowing-import-from :claw-as :mime-type)
  (:documentation "A comprehensive web application framework and server for the Common Lisp programming language")
  (:export #:*html-4.01-strict*
           #:*html-4.01-transitional*
           #:*html-4.01-frameset*
           #:*xhtml-1.0-strict*
           #:*xhtml-1.0-transitional*
           #:*xhtml-1.0-frameset*
           #:*rewind-parameter*
           #:*validation-errors*
           #:*claw-current-page*
           #:*claw-this-component*
           #:string-to-ps-symbol
           #:error-page
           #:render-error-page

           ;#:duplicate-back-slashes
           #:attribute-value
           #:build-tagf
           #:page
           #:page-before-render
           #:page-after-render
           #:template
           #:page-render
           #:make-page-renderer
           #:page-current-form
           #:page-req-parameter
           #:page-script-files
           #:page-stylesheet-files
           #:page-global-initscripts
           #:page-initscripts
           #:page-initstyles
           #:page-prerendering-p
           #:page-rendering-p
           #:current-component
           #:page-body-initscripts
           #:htcomponent
           #:htcomponent-page
           #:htcomponent-body
           #:htcomponent-empty
           #:htcomponent-client-id
           #:htcomponent-real-id
           #:htcomponent-script-files
           #:htcomponent-stylesheet-files
           #:htcomponent-global-initscripts
           #:htcomponent-initscripts
           #:htcomponent-initstyles
           #:tag
           #:tag-name
           #:tag-attributes
           #:htbody
           #:htscript
           #:htlink
           #:hthead
           #:htstring
           #:$>
           #:$raw>
           #:htignore
           #:ignore>
                                        ;empty tags definition
           #:*empty-tags*
           #:area>
           #:base>
           #:basefont>
           #:br>
           #:col>
           #:frame>
           #:hr>
           #:img>
           #:input>
           #:isindex>
           #:link>
           #:meta>
           #:param>
                                        ;standard tags
           #:a>
           #:abbr>
           #:acronym>
           #:address>
           #:applet>
           #:b>
           #:bdo>
           #:big>
           #:blockquote>
           #:body>
           #:button>
           #:caption>
           #:center>
           #:cite>
           #:code>
           #:colgroup>
           #:dd>
           #:del>
           #:dfn>
           #:dir>
           #:div>
           #:dl>
           #:dt>
           #:em>
           #:fieldset>
           #:font>
           #:form>
           #:frameset>
           #:h1>
           #:h2>
           #:h3>
           #:h4>
           #:h5>
           #:h6>
           #:head>
           #:html>
           #:i>
           #:iframe>
           #:ins>
           #:kbd>
           #:label>
           #:legend>
           #:li>
           #:map>
           #:menu>
           #:noframes>
           #:noscript>
           #:object>
           #:ol>
           #:optgroup>
           #:option>
           #:p>
           #:pre>
           #:q>
           #:s>
           #:samp>
           #:script>
           #:select>
           #:small>
           #:span>
           #:strike>
           #:strong>
           #:style>
           #:sub>
           #:sup>
           #:table>
           #:tbody>
           #:td>
           #:textarea>
           #:tfoot>
           #:th>
           #:thead>
           #:title>
           #:tr>
           #:tt>
           #:u>
           #:ul>
           #:var>
           #:parse-claw-template
           ;; class modifiers
           #:page-content
           #:generate-id
           #:metacomponent
           #:wcomponent
           #:wcomponent-created
           #:wcomponent-informal-parameters
           #:wcomponent-allow-informal-parametersp
           #:wcomponent-template
           #:wcomponent-before-rewind
           #:wcomponent-after-rewind
           #:wcomponent-before-prerender
           #:wcomponent-after-prerender
           #:wcomponent-before-render
           #:wcomponent-after-render
           #:cform
           #:action-object
           #:action
           #:form-method
           #:cform>
           #:action-link
           #:action-link>
           #:action-link-parameters
           #:translated-value
           #:cinput
           #:cinput>
           #:ctextarea
           #:ctextarea>
           #:cinput-file
           #:cinput-file>
           #:cinput-result-as-list-p
           #:ccheckbox
           #:ccheckbox>
           #:cradio
           #:cradio>
           #:cselect
           #:cselect>
           #:csubmit
           #:csubmit>
           #:csubmit-value
           #:submit-link
           #:submit-link>
           #:input-type
           #:ccheckbox-value
           #:css-class
           #:name-attr
           
           #:component-exceptions
           #:*id-and-static-id-description*
           
           #:describe-component-behaviour
           #:describe-html-attributes-from-class-slot-initargs
           
           ;;validation
           #:translator
           #:validation-error-control-string
           #:translator-integer
           #:translator-number
           #:translator-boolean
           #:translator-date
           #:translator-file
           #:translator-encode
           #:translator-decode
           #:translator-string-to-type
           #:translator-type-to-string
           #:translator-value-decode
           #:translator-value-encode
           #:translator-value-string-to-type
           #:translator-value-type-to-string
           #:*simple-translator*
           #:*boolean-translator*
           #:*integer-translator*
           #:*number-translator*
           #:*date-translator-ymd*
           #:*date-translator-time*
           #:*file-translator*
           #:validate
           #:add-validation-error
           #:component-validation-errors
           #:validate-required
           #:validate-size
           #:validate-range
           #:validate-number
           #:validate-integer
           #:validate-date-range
           #:exception-monitor
           #:exception-monitor>
           #:global-initscript
           #:global-initscript>
           #:initscript
           #:initscript>
           #:initstyle
           #:initstyle>))
