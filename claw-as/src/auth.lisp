;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; $Header: src/auth.lisp $

;;; Copyright (c) 2008, Andrea Chiumenti.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :claw-as)

;;------------------------------------------------------------------------------------------

(defgeneric configuration-login (configuration)
  (:documentation "Authenticate a user creating a principal object that will be stored into the http session.
If no session is present one will be created, if the authentication succeds the principal instance is returned"))


(defclass configuration ()
  ()
  (:documentation "A configuration class for CLAW server realm login configurations"))

(defmethod configuration-login ((configuration configuration))
  nil)

(defclass principal ()
  ((name :initarg :name
	 :reader principal-name
	 :documentation "The principal username who is logged into the application")
   (roles :initarg :roles
	  :accessor principal-roles
	  :documentation "The roles where that owns the user logged into the application"))
  (:default-initargs :roles nil)
  (:documentation "An instance of PRINCIPAL is stored into session after a user successfully login into the application."))


(defun login ()
  "Performs user authentication for the reaml where the request has been created"
  (let* ((login-config (gethash *claw-current-realm* (claw-server-login-config *claw-server*))))
    (when (and login-config (null (current-principal)))
      (setf (current-principal) (configuration-login login-config)))))