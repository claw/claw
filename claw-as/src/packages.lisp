;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; $Header: src/package.lisp $

;;; Copyright (c) 2008, Andrea Chiumenti.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :cl-user)


(defpackage :claw-as
  (:use :cl 
        :alexandria :cl-ppcre :local-time :split-sequence :bordeaux-threads :md5 :claw-i18n)
  (:shadow :flatten)
  (:import-from :cl-fad :directory-pathname-p)
  (:documentation "A comprehensive web application framework and server for the Common Lisp programming language")
  (:export #:*claw-server-base-path*
           #:*apache-http-port*
           #:*apache-https-port*
           #:*claw-default-server-address*
           #:*claw-server*
           #:*session-manager*
           #:*claw-current-lisplet*
           #:*claw-current-realm*
           #:*claw-session*
           #:claw-request-method
           #:claw-script-name
           #:claw-request-uri
           #:claw-query-string
           #:claw-get-parameter
           #:claw-get-parameters
           #:claw-post-parameter
           #:claw-post-parameters
           #:claw-parameter
           #:claw-header-in
           #:claw-headers-in
           #:claw-authorization
           #:claw-host-name
           #:claw-host-port
           #:claw-host-protocol
           #:claw-proxified-p
           #:claw-remote-addr
           #:claw-remote-port
           #:claw-real-remote-addr
           #:claw-server-addr
           #:claw-server-port
           #:claw-user-agent
           #:claw-referer
           #:claw-cookie-in
           #:claw-cookies-in
           #:claw-aux-request-value
           #:claw-delete-aux-request-value
           #:claw-content-type
           #:claw-header-out
           #:claw-headers-out
           #:claw-cookie-out
           #:claw-cookies-out
           #:claw-return-code
           #:claw-reply-external-format-encoding
           #:claw-writer
           #:claw-redirect
           #:claw-session-value
           #:claw-start-session
           #:claw-remove-session
           #:claw-delete-session-value
           #:log-message
           #:claw-cookie
           #:claw-cookie-name
           #:claw-cookie-value
           #:claw-cookie-expires
           #:claw-cookie-path
           #:claw-cookie-domain
           #:claw-cookie-secure
           #:claw-cookie-http-only

           #:connector
           #:connector-host
           #:connector-request-method
           #:connector-script-name
           #:connector-request-uri
           #:connector-query-string
           #:connector-get-parameter
           #:connector-get-parameters
           #:connector-post-parameter
           #:connector-post-parameters
           #:connector-parameter
           #:connector-header-in
           #:connector-headers-in
           #:connector-authorization
           #:connector-remote-addr
           #:connector-remote-port
           #:connector-real-remote-addr
           #:connector-server-addr
           #:connector-server-port
           #:connector-server-protocol
           #:connector-user-agent
           #:connector-referer
           #:connector-cookie-in
           #:connector-cookies-in
           #:connector-aux-request-value
           #:connector-delete-aux-request-value
           #:connector-header-out
           #:connector-headers-out
           #:connector-cookie-out
           #:connector-cookies-out
           #:connector-return-code
           #:connector-content-type
           #:connector-reply-external-format-encoding
           #:connector-writer
           #:connector-redirect
           #:connector-content-length
           #:connector-port
           #:connector-protocol
           #:connector-ssl-protocol
           #:connector-sslport
           #:connector-address
           #:connector-serving-port

           #:logger
           #:logger-log

           #:session-manager
           #:default-session-manager
           #:error-renderer

           #:mime-type
           #:duplicate-back-slashes
           #:lisplet
           #:lisplet-log-manager
           #:lisplet-server-address
           #:lisplet-error-handlers
           #:lisplet-pages
           #:lisplet-register-function-location
           #:lisplet-register-resource-location
           #:lisplet-register-page-location
           #:lisplet-protect
           #:lisplet-authentication-type
           #:lisplet-reverse-proxy-path
           #:lisplet-dispatch-request
           #:lisplet-message-dispatcher

           ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;#:build-lisplet-location
           #:*root-path*
           #:*context-path*
           #:*server-path*
           ;; claw-service
           #:claw-service
           #:claw-service-name
           #:claw-service-start
           #:claw-service-stop
           #:claw-service-running-p
           ;; claw-server
           #:claw-server
           #:claw-server-start
           #:claw-server-stop
           #:claw-server-connector
           #:claw-server-dispatch-method
           #:claw-server-log-manager
           #:claw-server-add-service
           #:claw-server-base-path
           #:claw-server-register-lisplet
           #:claw-server-unregister-lisplet
           #:claw-server-login-config

           #:claw-server-register-configuration

           #:configuration
           #:configuration-login
           
           #:claw-return
           #:principal
           #:current-principal
           #:current-config
           #:principal-name
           #:principal-roles
           #:user-locale
           #:user-in-role-p
           #:login
           #:register-library-resource
           #:serving-port
           #:serving-protocol
           #:modify-server-url))