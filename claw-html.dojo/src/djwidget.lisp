;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; $Header: dojo/src/djwidget.lisp $

;;; Copyright (c) 2008, Andrea Chiumenti.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :dojo)

(defgeneric djwidget-formal-parameters (djwidget)
  (:documentation "list of html attributes defined by widget slots"))

(defclass djwidget (wcomponent)
  ((tag-name :initarg :tag-name
	     :reader djwidget-tag-name
	     :documentation "The HTML tag element that will be rendered")
   (dojo-type :initarg :dojo-type
	      :reader djwidget-dojo-type
	      :documentation "The type of the dojo element, it will be added as dojoType HTML custom tag attribute")
   (dojo-rquire :initarg :dojo-require
		:reader djwidget-dojo-require
		:documentation "A list of addictional dojo reqirements"))
  (:metaclass metacomponent)
  (:default-initargs :tag-name "div" :dojo-require nil)
  (:documentation "Base class to render dojo widgets"))

(let ((class (find-class 'djwidget)))
  (closer-mop:ensure-finalized class)
  (setf (documentation (find-symbol (format nil "~a>" (class-name class))) 'function)
	(format nil "Description: ~a~%Parameters:~%~a~a~%~%~a"
		"Function that instantiates a DJWIDGET component and renders a html tag enabled for dojo whose name is provided by the :TAG-NAME keyword and the dojo widget by :DOJO-TYPE."
		*id-and-static-id-description*
		(describe-html-attributes-from-class-slot-initargs class)
		(describe-component-behaviour class))))

(defmethod djwidget-formal-parameters ((djwidget djwidget))())

(defmethod htcomponent-global-initscripts ((obj djwidget))
  (let ((dojo-type (djwidget-dojo-type obj))
	(dojo-require (djwidget-dojo-require obj)))
    (append
     (list (ps* `((getprop dojo 'require) "dojo.parser")))
     (unless dojo-require
       (list (ps* `((getprop dojo 'require) ,dojo-type))))
     (loop for require in dojo-require
        collect (ps* `((getprop dojo 'require) ,require))))))

(defmethod wcomponent-template ((obj djwidget))
  (let ((tag-name (djwidget-tag-name obj)))
    (when tag-name
      (let ((parameters (nconc (list :static-id (htcomponent-client-id obj) :dojo-type (djwidget-dojo-type obj))
                               (djwidget-formal-parameters obj))))
        (build-tagf tag-name
                    'tag
                    (not (null (find tag-name *empty-tags*)))
                    (list
                     parameters
                     (wcomponent-informal-parameters obj)
                     (htcomponent-body obj)))))))


(defclass dojo-require (wcomponent)
  ()
  (:metaclass metacomponent)
  (:documentation "Instructs the system to add a dojo.require clause to the rendered html page"))

(defmethod htcomponent-global-initscripts ((obj dojo-require))
  (let ((dojo-require (htcomponent-body obj)))
    (loop for require in dojo-require
       collect (ps* `((getprop dojo 'require) ,require)))))

(defmethod wcomponent-template ((obj dojo-require))
  nil) 