;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; $Header: dojo/src/djbody.lisp $

;;; Copyright (c) 2008, Andrea Chiumenti.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :dojo)

(defclass djbody (wcomponent)
  ((class :initarg :class
          :reader djbody-class
          :documentation "The css class of the <body> tag element")
   (theme :initarg :theme
          :reader djbody-theme
          :documentation "The theme name. See http://dojotoolkit.org/book/dojo-book-0-9/part-2-dijit/themes-and-design for more details")
   (themes-url :initarg :themes-url
               :reader djbody-themes-url
               :documentation "The url that contains dojo themes")
   (parse-on-load-p :initarg :parse-on-load
                    :reader djbody-parse-on-load-p
                    :documentation "Shoul always be true")
   (debugp :initarg :is-debug
           :reader djbody-debugp
           :documentation "Set to true if you want to debug dojo calls")
   (load-dojo-js :initarg :load-dojo-js
                 :reader load-dojo-js
                 :documentation "When not nil it loads the dojo.js file with a <script> tag. So that you can define alternative dojo sources")
   (include-css :initarg :include-css
                :reader djbody-include-css-p)
   (djconfig :initarg :djconfig
             :reader djbody-djconfig
             :documentation "Additional dojo configurations"))
  (:metaclass metacomponent)
  (:default-initargs :class "" :theme "tundra"
                     :themes-url (format nil "~a/dojotoolkit/dijit/themes/" *server-path*)
                     :parse-on-load "true"
                     :load-dojo-js t
                     :is-debug nil
                     :include-css t
                     :djconfig nil)
  (:documentation "This class provide a <body> tag that is enabled for dojo."))

(let ((class (find-class 'djbody)))
  (closer-mop:ensure-finalized class)
  (setf (documentation (find-symbol (format nil "~a>" (class-name class))) 'function)
        (format nil "Description: ~a~%Parameters:~%~a~a~%~%~a"
                "Function that instantiates a DJBODY component and renders a html <body> tag enabled for dojo."
                *id-and-static-id-description*
                (describe-html-attributes-from-class-slot-initargs class)
                (describe-component-behaviour class))))

(defmethod htcomponent-script-files ((o djbody))
  (let ((parse-on-load (djbody-parse-on-load-p o))
        (is-debug (djbody-debugp o))
        (djconfig (djbody-djconfig o)))
    (when (load-dojo-js o)
      (script> :type "text/javascript"
               :src  (format nil "~a/dojotoolkit/dojo/dojo.js" *server-path*)
               :djconfig (if (null djconfig)
                             (format nil
                                     "parseOnLoad:~a,usePlainJson:true,isDebug:~:[false~;true~]"
                                     parse-on-load is-debug)
                             (format nil
                                     "parseOnLoad:~a,usePlainJson:true,~a,isDebug:~:[false~;true~]"
                                     parse-on-load djconfig is-debug))))))

(defmethod htcomponent-stylesheet-files ((o djbody))
  (when (djbody-include-css-p o)
    (let ((theme (djbody-theme o)))
      (list
       (format nil "~a/dojotoolkit/dojo/resources/dojo.css" *server-path*)
       (format nil "~a/dojotoolkit/dijit/themes/dijit.css" *server-path*)
       (format nil "~a~a/~a.css" (djbody-themes-url o) theme theme)))))


(defmethod djbody-cssclass ((o djbody))
  (format nil "~a ~a" (djbody-theme o) (djbody-class o)))

(defmethod wcomponent-template ((obj djbody))
  (let ((id "scripts-content-pane")
        (pobj (htcomponent-page obj))
        (attributes (append (list :class (djbody-cssclass obj))
                            (wcomponent-informal-parameters obj))))
    (build-tagf "body" 'tag nil
                attributes
                (htcomponent-body obj)
                (dojo-require> "dojox.layout.ContentPane")
                (div> :static-id id
                      :dojo-type "dojox.layout.ContentPane"
                      :execute-scripts "true"
                      :render-styles "true"
                      (script> :type "text/javascript"
                               (page-body-initscripts pobj))))))


(defmethod wcomponent-after-prerender ((obj djbody) (pobj page))
  (let ((scripts (page-initscripts pobj)))
    ;;remember that scripts are in reverse order
    (when scripts
      (push "});" (page-initscripts pobj))
      (nconc scripts (list "dojo.addOnLoad\(function\() {")))))


