/**
;;; $Header: src/js/Form.js $

;;; Copyright (c) 2008, Andrea Chiumenti.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


        dojo.provide("claw.Form");

        dojo.require("dojo.io.iframe");
        dojo.require("dijit.form.Form");

        dojo.declare(
	    "claw.Form",
	    [dijit.form.Form],
	    {
		// summary:
		// Adds conveniences to regular HTML form

		// HTML <FORM> attributes
                xhrTimeout: "",//2000,
                updateId: null,
                enctype: "",
                xhr: null,
                action: "#",
                jsonContent: {},
		_updateParts: function (reply) {
                    for (var item in reply.components) {
                        var element = dojo.byId(item);
                        if (element != null) {
                            if (reply.components[item] != null) {
                                var list = dojo.query('[dndId]', element);
                                dojo.forEach(list, function(dndEl){
                                    var dndObj = claw.dnd.byId(dojo.attr(dndEl, "dndId"));
                                    if (dndObj) {
                                        dndObj.destroy(); 
                                    }
                                });
                                list = dojo.query('[widgetId]', element);
                                dojo.forEach(list.map(dijit.byNode), function(widget){if (widget) widget.destroy(); });
                            }
                            element.innerHTML = reply.components[item];
                            dojo.parser.parse(element, true);
                        }
                    }
                },

                _evalReplClassScripts: function (reply) {
                    dijit.byId('scripts-content-pane').attr('content', reply.classInjections);
                },

                _evalReplInstanceScripts: function (reply) {
                    dijit.byId('scripts-content-pane').attr('content', reply.instanceInjections);
                },

                _updateAndEval: function (reply) {
                    //console.debug("Plain object as string is: ", reply);
                    //console.debug("Object as string is: ", dojo.toJson(reply, true));
                    this._evalReplClassScripts(reply);
                    this._updateParts(reply);
                    this._evalReplInstanceScripts(reply);
                },
                submit: function(){
                    if(!(this.onSubmit() === false) && !this.xhr){
                        this.containerNode.submit();
		    }
                },
		onSubmit: function(e){
		    //	summary:
		    //		Callback when user submits the form. This method is
		    //		intended to be over-ridden, but by default it checks and
		    //		returns the validity of form elements. When the `submit`
		    //		method is called programmatically, the return value from
		    //		`onSubmit` is used to compute whether or not submission
		    //		should proceed

		    var valid = this.validate(); // Boolean

                    if (valid && this.xhr) {
                        if (e) {
                            e.preventDefault();
                        }
                        this.onBeforeSubmit(e);
                        var thisForm = this;
                        var jsonContent = dojo.mixin(this.jsonContent, { json : thisForm.updateId });
                        this.jsonContent = {};
                        var formId = this.id;
                        if (this.enctype != 'multipart/form-data') {
                            try {
                                dojo.xhrPost({
                                    url: this.action,
                                    load : function (data) {
                                        try {
                                            thisForm._updateAndEval(data);
                                        } finally {
                                            thisForm.onXhrFinish(e);
                                        }
                                    },
                                    error : function (data) {console.error("!!!!!!",data);thisForm.onXhrFinish(e);},
                                    timeout : thisForm.xhrTimeout,
                                    handleAs : 'json',
                                    form : formId,
                                    content : jsonContent });
                            } catch (e) {alert(e);}
                        } else {
                            jsonContent = dojo.mixin(jsonContent, { jsonPrefix: '<textarea>', jsonSuffix: '</textarea>' });
                            dojo.io.iframe.send({
                                load : function (data) {
                                    try {
                                        thisForm._updateAndEval(data);
                                    } finally {
                                        thisForm.onXhrFinish(e);
                                    }
                                },
                                error : function (data) {
                                    console.error(data);
                                    thisForm.onXhrFinish(e);
                                },
                                timeout : thisForm.xhrTimeout,
                                handleAs : 'json',
                                form: formId,
                                content : jsonContent });
                        }
                    } 
                    this.jsonContent = {};
                    return valid;
		},

                onBeforeSubmit: function(/*Event?*/e){
		    //	summary:
		    //		Callback when user submits the form. This method is
		    //		intended to be over-ridden. When the `submit` calls dojo.xhrPost
		    //		this method is called before.
		},

                onXhrFinish: function(/*Event?*/e){
		    //	summary:
		    //		Callback when user submits the form. This method is
		    //		intended to be over-ridden. After the call to dojo.xhrPost
		    //		thouches lload or error this event is fired
		}
	    }
        );



