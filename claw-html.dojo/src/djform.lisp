;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; $Header: dojo/src/djform.lisp $

;;; Copyright (c) 2008, Andrea Chiumenti.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :dojo)

(defclass djform (cform djwidget)
  ((update-id :initarg :update-id
              :reader update-id
              :documentation "A list of the component id to update")
   (ajax-form-p :initarg :ajax-form-p
                :reader djform-ajax-form-p
                :documentation "When not nil, requests are sent via XHR call."))
  (:metaclass metacomponent)
  (:documentation "Class to generate a <form> element that is capable of XHR requests. More info at http://api.dojotoolkit.org/")
  (:default-initargs :dojo-type "claw.Form" :update-id () :ajax-form-p t))


(defmethod wcomponent-template :before ((obj djform))
  (let ((dojo-type (djwidget-dojo-type obj))
        (update-id (update-id obj)))
    (setf (wcomponent-informal-parameters obj) 
          (append (wcomponent-informal-parameters obj)
                  (list :xhr (djform-ajax-form-p obj)
                        :dojotype dojo-type
                        :update-id (when update-id
                                     (let ((js-array (if (listp update-id) 
                                                         (ps* `(array ,@update-id))
                                                         (ps* `(array ,update-id)))))
                                       (subseq js-array 0 (1- (length js-array))))))))))


(defmethod htcomponent-initscripts ((obj djform))
  nil)


